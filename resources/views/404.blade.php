<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>404</title>
  </head>
  <body>
    <link
      href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
      rel="stylesheet"
      id="bootstrap-css"
    />
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <div class="page-wrap d-flex flex-row align-items-center">
     <div class="container">
        <div class="center text-center"></div>

        <div class="frame d-flex">
          <div class="align-self-center text-center kg">
            <!-- <iomg src="/assets/images/406.png" alt="" width="150" /> -->
            <iomg src="/assets/images/406-2.png" alt width="100" />
            <h2 class>
              ไม่พบรายการบ้านในระบบ
            </h2>
          </div>
        </div>
      </div>
    </div>
  </body>
  <style>
    body {
      background: #dedede;
    }

    .page-wrap {
      min-height: 100vh;
    }

    body {
  background-image: none !important;
}
.container {
  padding-right: 35px;
  padding-left: 35px;
  display: flex;
  height: 100vh;
  justify-content: center;
  align-items: center;
  height: calc(100vh - 30px);
  // position: relative;
  // display: table;
  // height: 100%;
  // background-color: #fff;
}
.btn-primary {
  padding-top: 8px;
  padding-bottom: 30px;
  background-color: #f37022;
  border-color: #f37022;
  border-radius: 5px;
}
.m-btn {
  position: relative;
  color: #fff;
  width: 100%;
  background-color: #fff;
  border-radius: 5px;
  padding-top: 5px;
  padding-bottom: 5px;
  box-shadow: 0px 3px 2px 0px rgba(0, 0, 0, 0.44);
}
.btn-grey {
  color: #fff;
  background-color: #636466;
  border-color: #636466;
  width: 100%;
  border-radius: 5px;
  padding-top: 5px;
  padding-bottom: 5px;
  box-shadow: 0px 3px 2px 0px rgba(0, 0, 0, 0.44);
  font-size: 25px;
  i {
    font-size: 10px;
  }
}
.btn-orange {
  color: #fff;
  background-color: #f37022;
  border-color: #f37022;
  width: 100%;
  border-radius: 5px;
  padding-top: 5px;
  padding-bottom: 5px;
  box-shadow: 0px 3px 2px 0px rgba(0, 0, 0, 0.44);
  font-size: 25px;
  i {
    font-size: 17px;
    margin-right: 5px;
  }
}
.imageProfile img {
  border: 3px solid #f37022;
  border-radius: 50%;
}
h2 {
  font-weight: 600;
  font-size: 25px;
  line-height: 1.5;
  //letter-spacing: 4px;
}
span {
  font-size: 30px;
  color: #00b900;
}
button {
  font-size: 6vw;
}
  </style>
</html>
