<table>
  <thead>
    <tr>
      <th>ชื่อ</th>
      <th>นามสกุล</th>
      <th>อีเมล</th>
      <th>เบอร์โทร</th>
      <th>Line ID</th>
      <th>Created at</th>

      @for ($i = 1; $i <= 10; $i++)
      <th>ชื่อบ้าน {{ $i }}</th>
      <th>CA {{ $i }}</th>
      <th>UI {{ $i }}</th>
      @endfor
    </tr>
  </thead>

  <tbody>
    @foreach($users as $user)

    <tr>
      <td>{{ isset($user->first_name) ? strval($user->first_name) : '' }}</td>
      <td>{{ isset($user->last_name) ?strval($user->last_name) : '' }}</td>
      <td>{{ isset($user->email) ?strval($user->email): '' }}</td>
      <td>{{ isset($user->tel) ? strval($user->tel) : '' }}</td>
      <td>{{ isset($user->line_user_id) ? strval($user->line_user_id) : '' }}</td>
      <td>{{ isset($user->created_at) ? strval($user->created_at) : ''}}</td>

      @foreach($user->homes as $home)
      <td>{{ strval($home->name) }}</td>
      <td>{{ strval($home->ca) }}</td>
      <td>{{ strval($home->ui) }}</td>
      @endforeach
    </tr>
    @endforeach
  </tbody>
</table>
