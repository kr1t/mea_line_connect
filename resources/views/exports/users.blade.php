<table>
  <thead>
    <tr>
      <th>ชื่อ</th>
      <th>นามสกุล</th>
      <th>อีเมล</th>
      <th>เบอร์โทร</th>
      <th>Line ID</th>
      <th>Created at</th>
    </tr>
  </thead>

  <tbody>
    @foreach($users as $user)

    <tr>
      <td>{{ isset($user->first_name) ? strval($user->first_name) : '' }}</td>
      <td>{{ isset($user->last_name) ? strval($user->last_name) : ''  }}</td>
      <td>{{ isset($user->email) ? strval($user->email) : '' }}</td>
      <td>{{ isset($user->tel) ? strval($user->tel) : ''  }}</td>
      <td>{{ isset($user->line_user_id) ? strval($user->line_user_id) : '' }}</td>
      <td>{{ isset($user->created_at) ? strval($user->created_at)  : ''}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
