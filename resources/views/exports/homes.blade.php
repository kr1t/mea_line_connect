<table>
  <thead>
    <tr>
      <th>ชื่อบ้าน</th>
      <th>CA</th>
      <th>UI</th>
      <th>Line ID</th>
      <th>Created at</th>
    </tr>
  </thead>

  <tbody>
    @foreach($homes as $home)
    <tr>
      <td>{{ isset($home->name) ? strval($home->name): '' }}</td>
      <td>{{ isset($home->ca) ?strval($home->ca): '' }}</td>
      <td>{{ isset($home->ui) ? strval($home->ui) : '' }}</td>
      <td>{{ isset($home->user) ? strval($home->user->line_user_id) : ''}}</td>
      <td>{{ isset($home->created_at) ? strval($home->created_at) : '' }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
