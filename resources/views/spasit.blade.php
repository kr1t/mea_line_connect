@php $config = [ 'appName' => config('app.name'), 'locale' => $locale = app()->getLocale(),
'locales' => config('app.locales'), 'githubAuth' => config('services.github.client_id'), 'isLocal'=>
env('APP_ENV') == 'local', 'fdir'=> env('FDIR') ? env('FDIR') : '', ]; @endphp @php $fdir =
$config['fdir'] ? '/'. $config['fdir'] : ''; @endphp


@php

$config['liff']= Helpers\liffAll(); @endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1,  maximum-scale=1.0,shrink-to-fit=no" />

  <title>{{ config('app.name') }}</title>
  {{--
  <title>MEA CONNECT</title>
  --}}
  <link rel="stylesheet" href="{{ $fdir }}{{ ('/test/dist/css/app.css') }}?v=2" />
  <!-- <script src="https://kit.fontawesome.com/8f9e6c018f.js" crossorigin="anonymous"></script> -->
  <!-- import CSS -->
</head>

<body>
  <div id="app"></div>

  {{-- Global configuration object --}}
  <script>
    window.config = @json($config);
  </script>

  {{-- Load the application scripts --}}
  <script src="{{ $fdir }}{{ ('/test/dist/js/app.js') }}?v=2"></script>

  <style>
    body {
      font-family: 'db_ozone_xmedium';
    }

    @font-face {
      font-family: 'db_ozone_xmedium';
      src: url('{{ $fdir }}/test/assets/fonts/db_ozone_x_med_v3.2-webfont.woff2') format('woff2'),
      url('{{ $fdir }}/test/assets/fonts/db_ozone_x_med_v3.2-webfont.woff') format('woff'),
      url('{{ $fdir }}/test/assets/fonts/db_ozone_x_med_v3.2-webfont.ttf') format('truetype');
      font-weight: normal;
      font-style: normal;
    }

    @font-face {
      font-family: 'db_ozone_xlight';
      src: url('{{ $fdir }}/test/assets/fonts/db_ozone_x_li_v3.2-webfont.woff2') format('woff2'),
      url('{{ $fdir }}/test/assets/fonts/db_ozone_x_li_v3.2-webfont.woff') format('woff'),
      url('{{ $fdir }}/test/assets/fonts/db_ozone_x_li_v3.2-webfont.ttf') format('truetype');
      font-weight: normal;
      font-style: normal;
    }

    .container {
      min-height: 100%;
      /* Put height of the footer here. Needed for higher than screen height pages */
    }

    html {
      height: 100%;
    }

    body {
      min-height: 100%;
    }

    .btn.disabled,
    .btn:disabled {
      opacity: 1;
      background-color: #ccc !important;
      color: #333 !important;
      border: #ccc;
    }

    .st-b {
      text-shadow: rgb(255, 255, 255) 2.5px 0px 0px, rgb(255, 255, 255) 1.75517px 0.958851px 0px,
        rgb(255, 255, 255) 1.0806px 1.68294px 0px, rgb(255, 255, 255) 0.141474px 1.99499px 0px,
        rgb(255, 255, 255) -0.832294px 1.81859px 0px, rgb(255, 255, 255) -1.60229px 1.19694px 0px,
        rgb(255, 255, 255) -1.97998px 0.28224px 0px, rgb(255, 255, 255) -1.87291px -0.701566px 0px,
        rgb(255, 255, 255) -1.30729px -1.5136px 0px, rgb(255, 255, 255) -0.421592px -1.95506px 0px,
        rgb(255, 255, 255) 0.567324px -1.91785px 0px, rgb(255, 255, 255) 1.41734px -1.41108px 0px,
        rgb(255, 255, 255) 1.92034px -0.558831px 0px;
    }

    /* .main-layout {
        background-image: url('{{ $fdir }}/test/assets/images/bg-footer.png');
        background-position: center bottom;
        background-repeat: no-repeat;
        background-size: 100%;
      } */

    .swal2-styled.swal2-confirm {
      background-color: #f37022;
    }
  </style>
</body>

</html>
