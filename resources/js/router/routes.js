function page(path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}
let baseDir = window.config.fdir
baseDir = baseDir ? baseDir : ''
export default [
  {
    path: `/${baseDir}`,
    component: () => import(`~/layouts/empty`),
    children: [
      { path: '', name: 'welcome', component: page('welcome.vue') },

      { path: 'register', name: 'register', component: page('auth/register.vue') },
      {
        path: 'register/sticker',
        name: 'register.sticker',
        component: page('authSticker/register.vue')
      },
      {
        path: 'register/delete',
        name: 'register.delete',
        component: page('authSticker/delete.vue')
      },
      { path: 'admin/login', name: 'admin.login', component: page('admin/login') },

      {
        path: 'admin',
        component: () => import(`~/pages/admin/admin`),
        children: [
          { path: '', redirect: { name: 'admin.home' } },
          {
            path: 'home',
            name: 'admin.home',
            component: page('admin/home')
          },
          {
            path: 'data/export',
            name: 'admin.export',
            component: page('admin/dataExport')
          },
          {
            path: 'stats',
            name: 'admin.stats',
            component: page('admin/stats')
          },
          {
            path: 'houses',
            name: 'admin.houses',
            component: page('admin/houses')
          },
          {
            path: 'ebill',
            name: 'admin.ebill',
            component: page('admin/ebill')
          },
          {
            path: 'api_docs',
            name: 'admin.api',
            component: page('admin/apiDocs')
          },
          {
            path: 'add/user',
            name: 'admin.add.user',
            component: page('admin/addUser')
          },
          {
            path: 'users',
            name: 'admin.user',
            component: page('admin/user')
          },
          {
            path: 'reset/password',
            name: 'admin.reset.password',
            component: page('admin/resetPassword')
          }
        ]
      },
      // {
      //   path: 'register/policy',
      //   name: 'Policy',
      //   component: page('auth/Policy')
      // },

      {
        path: 'home/create',
        name: 'AddHome',
        component: page('home/AddHome')
      },
      {
        path: 'home/success',
        name: 'RegisterSuccess',
        component: page('home/HomeSuccess')
      },
      {
        path: 'home/homelist',
        name: 'HomeList',
        component: page('home/HomeList')
      },
      {
        path: 'home/homelist/history',
        name: 'HomeHistory',
        component: page('home/HomeHistory')
      },
      {
        path: 'home/homelist/id',
        name: 'BillDetail',
        component: page('home/BillDetail')
      },
      // {
      //   path: 'home/homelist/detailprice',
      //   name: 'DetailPrice',
      //   component: page('home/DetailPrice')
      // },
      {
        path: 'home/setting',
        name: 'HomeSetting',
        component: page('home/HomeSetting')
      },
      {
        path: 'home/edit',
        name: 'HomeAlert',
        component: page('home/HomeAlert')
      },
      {
        path: 'home/edit/success',
        name: 'HomeEDSuccess',
        component: page('home/HomeEDSuccess')
      },
      {
        path: 'home/delete',
        name: 'HomeDelAlert',
        component: page('home/HomeDelAlert')
      },

      //report//

      {
        path: 'report',
        name: 'Report',
        component: page('report/Report')
      },
      {
        path: 'report/home',
        name: 'ReportHome',
        component: page('report/ReportHome')
      },
      {
        path: 'report/success',
        name: 'ReportSuccess',
        component: page('report/ReportSuccess')
      },

      //document//

      {
        path: 'document',
        name: 'HomeDoc',
        component: page('document/HomeDoc')
      },

      // {
      //   path: 'document-test',
      //   name: 'HomeDocTest',
      //   component: page('document/HomeDocTest')
      // },
      {
        path: 'document/history',
        name: 'DocHistory',
        component: page('document/DocHistory')
      },
      {
        path: 'document/success',
        name: 'DocSuccess',
        component: page('document/DocSuccess')
      },

      //management//
      {
        path: 'setting/user',
        name: 'MUser',
        component: page('manage/MUser')
      },
      {
        path: 'setting/success',
        name: 'MSuccess',
        component: page('manage/MSuccess')
      },
      {
        path: 'setting/owner',
        name: 'MOwner',
        component: page('manage/MOwner')
      },
      {
        path: 'setting/staff',
        name: 'MStaff',
        component: page('manage/MStaff')
      },
      {
        path: 'setting/outsource',
        name: 'MOutSource',
        component: page('manage/MOutSource')
      },
      {
        path: 'setting/homeset',
        name: 'HomeSett',
        component: page('manage/HomeSett')
      },
      // payment
      {
        path: 'payment/',
        name: 'Payment',
        component: page('payment/Payment')
      },

      {
        path: 'paymentTest/',
        name: 'PaymentTest',
        component: page('paymentTest/Payment')
      },
      {
        path: 'verify/',
        name: 'verify',
        component: page('verifyAccount/verifyAccount')
      },

      { path: 'home', name: 'home', component: page('home.vue') },

      { path: 'error406', name: 'error406', component: page('errors/406.vue') },
      // { path: '*', component: page('errors/404.vue') }
    ]
  }
]
