import * as types from '../mutation-types'
import axios from 'axios'
import liff from '@line/liff'
import path from '~/path'

export const state = {
  home: {
    histories: []
  },
  month_name: '',
  currentPayment: 1,
  test: ''
}

// getters
export const getters = {
  home: state => state.home,
  bill_detail: state => state.home.histories.find(el => el.month_name == state.month_name),
  currentPayment: state => state.currentPayment
}

// mutations
export const mutations = {
  [types.HOME_SHOW](state, home) {
    state.home = home
  },
  [types.HOME_HISTORY](state, histories) {
    state.home.histories = histories
  },
  [types.HOME_HISTORY_DETAIL](state, month_name) {
    state.month_name = month_name
  },
  [types.HOME_CP](state, currentPayment) {
    state.currentPayment = currentPayment
  },
  [types.HOME_TEST](state, test) {
    state.test = test
  }
}

// actions
export const actions = {
  async setHomeTest({ commit }, test) {
    commit(types.HOME_TEST, test)
  },
  async show({ commit, state }, meterNo) {
    // console.log('start load home')

    let homeName = ''
    let home = {
      bill: { description: {} }
    }
    // console.log('start api load home')

    const { data } = await axios.get(path.home.show(meterNo, state.test)).catch(e => { })
    // console.log('after api load home')

    const { customer, nobil, home_detail } = data

    const homeD = data.home

    // console.log('home ', homeDetail)
    home.tel = customer.tel ?? ''
    home.nobill = nobil
    home.customer_name = customer.name
    home.address = customer.address

    home.name = homeD.name
    home.ebill_email = homeD.ebill_email

    home.id = homeD.id
    home.no = ''
    home.bill.ca = homeD.ca
    home.bill.description.amount = home_detail.amount

    if (!nobil) {
      home.bill.due_date_text = home_detail.due_date
      // "20200814"
      let y = home.bill.due_date_text.substring(0, 4)
      let m = home.bill.due_date_text.substring(4, 6)
      let d = home.bill.due_date_text.substring(6, 8)

      let mlist = {
        l01: 'มกราคม',
        l02: 'กุมภาพันธ์',
        l03: 'มีนาคม',
        l04: 'เมษายน',
        l05: 'พฤษภาคม',
        l06: 'มิถุนายน',
        l07: 'กรกฎาคม',
        l08: 'สิงหาคม',
        l09: 'กันยายน',
        l10: 'ตุลาคม',
        l11: 'พฤศจิกายน',
        l12: 'ธันวาคม'
      }
      let mm = mlist[`l${m}`]
      home.bill.due_date_text_r = `${d} ${mm} ${y}`
      home.bill.due_date_text_th = `${d}${m}${(+y + 543 + '').substring(4 - 2)}`

      let gM = home_detail.last_date.substring(4, 6)
      home.bill.last_date_text_r = mlist[`l${gM}`]
      home.bill.invoice_no = home_detail.invoice_no
      home.transNo = home_detail.TransNo

      home.bill.kwh = home_detail.kwh

      home.bill.DisconnetAmt = home_detail.DisconnetAmt
      home.bill.DiscNoVat = home_detail.DiscNoVat
      home.bill.DiscVatAmt = home_detail.DiscVatAmt
      home.bill.TotalVat = home_detail.TotalVat
      home.bill.TotalInt = home_detail.TotalInt
      home.bill.IntDay = home_detail.IntDay
      home.bill.IntAmt = home_detail.IntAmt
      home.bill.TotalBill = home_detail.TotalBill
      home.bill.TotAmt = home_detail.TotAmt
    }

    home.bill.installation = homeD.ui

    home.bill.dept_code = home_detail.dept_code
    home.request = data.home.request_e_document_at != null

    home.message = home_detail.pay_flag.message
    // console.log(home)z

    commit(types.HOME_SHOW, home)
  },

  async histories({ commit }, meterNo) {
    const { data } = await axios.get(path.home.history(meterNo)).catch()
    commit(types.HOME_HISTORY, data.result)

    return { data }
  },
  setMonthDetail({ commit }, month_name) {
    commit(types.HOME_HISTORY_DETAIL, month_name)
  },
  setCP({ commit }, cp) {
    commit(types.HOME_CP, cp)
  }
}
