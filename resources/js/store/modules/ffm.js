import * as types from '../mutation-types'
import Cookies from 'js-cookie'
import axios from 'axios'

let authenConfig = {
  username: 'chantima.t@cdg.co.th',
  password: '1234',
  grant_type: 'password',
  client_id: 'line'
}

const qs = require('querystring')

export const state = {
  token: Cookies.get('ffm_token')
}

// getters
export const getters = {
  token: state => state.token
}

// mutations
export const mutations = {
  [types.SAVE_TOKEN](state, { access_token, expires_in }) {
    state.token = access_token
    Cookies.set('ffm_token', access_token, { expires: expires_in })
  }
}

// actions
export const actions = {
  async saveToken({ commit }) {
    // console.log('ok')
    const { data } = await axios.post(
      'https://ffmdev.mea.or.th/ffmapi/api/appauthen/token',
      qs.stringify(authenConfig),
      {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    )
    commit(types.SAVE_TOKEN, data)
  }
}
