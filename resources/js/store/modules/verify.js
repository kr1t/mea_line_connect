import * as types from '../mutation-types'
import { countBy, filter } from 'lodash'
// state
export const state = {
  homes: []
}

// getters
export const getters = {
  homes: state => state.homes,
  activeCount: state => {
    return countBy(state.homes, (h) => {
      return h.active == true
    })
  },
  homeActives: state => {
    return filter(state.homes, (h) => {
      return h.active == true && h.added == false
    })
  }
}

// mutations
export const mutations = {
  SET_HOME(state, homes) {
    state.homes = homes
  },
  EDIT_HOME(state, home) {
    state.homes = state.homes.map((h) => {
      console.log({ h, home })
      if (h.contractNumber == home.contractNumber) {
        h.name = home.name
        h.active = home.active
      }
      return h
    })
    console.log('EDIT_HOME', home)
  }
}

// actions
export const actions = {
  setHomes({ commit }, payload) {
    commit('SET_HOME', payload)
  },
  editHomeName({ commit }, payload) {
    commit('EDIT_HOME', payload)
  },
  setStatusHome({ commit }, payload) {
    commit('EDIT_HOME', payload)
  }
}
