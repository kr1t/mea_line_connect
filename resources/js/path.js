const baseUrl = window.config.fdir ? '/' + window.config.fdir : ''
// console.log('apiPath', baseUrl)

const baseOrIn = baseUrl == '' ? 'http://192.168.118.20/lineconnect' : baseUrl
export default {
  user: {
    registerCheck: userId => `${baseUrl}/api/line/user/check/register?line_user_id=${userId}`,
    grantSticker: userId => `${baseUrl}/api/line/user/sticker/grant?line_user_id=${userId}`,
    delete: userId => `${baseUrl}/api/line/user/delete?line_user_id=${userId}`,
    register: `${baseUrl}/api/register`,
    update: `${baseUrl}/api/user/update`,
    pushStaff: user_id => `${baseUrl}/api/push/staff/${user_id}`,
    getProfile: user_id => `${baseUrl}/api/user/${user_id}`,
    otp: user_id => `${baseUrl}/api/otp?line_user_id=${user_id}`,
    otpConfirm: user_id => `${baseUrl}/api/otp_comfirm?line_user_id=${user_id}`
  },
  verify: {
    verify: `${baseUrl}/api/verify`,
    addHome: `${baseUrl}/api/verify/addHome`
  },
  home: {
    validate: (ca, ui) => `${baseUrl}/api/home_validate?ca=${ca}&ui=${ui}`,
    create: `${baseUrl}/api/home`,
    show: (meterNo, test) => `${baseUrl}/api/home/${meterNo}?test=${test}`,
    edocSub: meterNo => `${baseUrl}/api/edoc/${meterNo}`,
    history: meterNo => `${baseUrl}/api/home/${meterNo}/history`,
    del: meterNo => `${baseUrl}/api/home/${meterNo}/del`
  },
  ffm: {
    outage: `${baseUrl}/api/report/outage`,
    home: `${baseUrl}/api/report/home`
  },
  ebill: {
    check: meter_no => `${baseOrIn}/api/e-doc/check/${meter_no}`,
    checkTest: meter_no => `${baseOrIn}/api/e-doc/check-test/${meter_no}`,
    create: meter_no => `${baseOrIn}/api/e-doc/create/${meter_no}`,
    cancel: meter_no => `${baseOrIn}/api/e-doc/cancel/${meter_no}`
  },
  admin: {
    login: `${baseUrl}/api/login`,
    usersCreate: `${baseUrl}/api/users`,
    resetPassword: `${baseUrl}/api/settings/password`,

    usersRemoveAdmin: id => `${baseUrl}/api/users/del?id=${id}`,
    users: (page, q, isAdmin = 0) => `${baseUrl}/api/users?page=${page}&q=${q}&isAdmin=${isAdmin}`,
    houses: (page, q) => `${baseUrl}/api/home?page=${page}&q=${q}`,
    ebills: (page, q) => `${baseUrl}/api/ebill_logs?page=${page}&q=${q}`,
    dashboard: `${baseUrl}/api/dashboard`,
    export: (name, from, to) => `${baseUrl}/api/exports/${name}?from=${from}&to=${to}`
  },
  stats: {
    cr: `${baseUrl}/api/stats`
  },
  linepayGen: ui => `${baseUrl}/api/linepay/${ui}`,
  kplus: ui => `${baseUrl}/api/kplus/${ui}/push`,

  baseUrl: `${baseUrl}`
}
