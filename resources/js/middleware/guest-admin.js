import store from '~/store'

export default (to, from, next) => {
  console.log(store.getters['admin/check'])
  if (store.getters['admin/check']) {
    next({ name: 'admin.home' })
  } else {
    next()
  }
}
