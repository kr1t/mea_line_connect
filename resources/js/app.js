import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import Mixins from './mixins'
import ElementUI from 'element-ui'

Vue.mixin(Mixins)

import '~/plugins'
import '~/components'

Vue.config.productionTip = false
import liff from '@line/liff'
Vue.prototype.$liff = liff

import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faUserSecret,
  faEnvelope,
  faMobile,
  faPlusCircle,
  faCamera
} from '@fortawesome/free-solid-svg-icons'
// import * as  Kuy from '@fortawesome/free-solid-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faFontAwesome } from '@fortawesome/free-brands-svg-icons'
library.add(faUserSecret, faEnvelope, faMobile, faPlusCircle, faCamera)
import { ToggleButton } from 'vue-js-toggle-button'

Vue.component('ToggleButton', ToggleButton)
// library.add({ ...Kuy })
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueGeolocation from 'vue-browser-geolocation'
import Swal from 'sweetalert2'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI)
Vue.use(VueGeolocation)
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.component('pagination', require('laravel-vue-pagination'))

const queryString = decodeURIComponent(window.location.search).replace('?liff.state=', '')
const params = new URLSearchParams(queryString)
Vue.prototype.$liffParams = params
Vue.config.devtools = false
Vue.prototype.$swal = Swal /* eslint-disable no-new */
// Vue.publicPath = 'armddqwd'
import path from '~/path'
Vue.prototype.$path = path

import JsonExcel from 'vue-json-excel'

Vue.component('downloadExcel', JsonExcel)

import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
Vue.use(VueSidebarMenu)

import VuePrism from 'vue-prism'
Vue.use(VuePrism)

import 'prismjs/themes/prism.css'

import OtpInput from '@bachdgvn/vue-otp-input'

Vue.component('v-otp-input', OtpInput)

new Vue({
  i18n,
  store,
  router,
  ...App
})
