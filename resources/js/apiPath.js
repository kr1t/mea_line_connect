const baseUrl = ''

export default {
  user: {
    registerCheck: userId => `${baseUrl}/api/line/user/check/register?line_user_id=${userId}`,
    register: `${baseUrl}/api/register`
  }
}
