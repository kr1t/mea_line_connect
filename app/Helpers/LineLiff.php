<?php

namespace Helpers;

class LineLiff
{
    public $baseUrl = "https://liff.line.me/";
    private $data = [
        "reportOutage" => [
            "dev" => "1654946446-NLy1YOjy",
            "production" => "1654946446-NLy1YOjy",
        ],
        "addHome" => [
            "dev" => "1657634887-M6WV0ByZ",
            "production" => "1654946446-97p4drDp",
        ],
        "loginOwner" => [
            "dev" => "1654946446-NLy1YOjy",
            "production" => "1654946446-NLy1YOjy",
        ],
        "profile" => [
            "dev" => "1657634887-J6Rx06Gn",
            "production" => "1654946446-N5ao7zBa",
        ],
        "outsource" => [
            "dev" => "1654946446-PoGO9M4G",
            "production" => "1654946446-PoGO9M4G",
        ],
        "register" => [
            "dev" => "1657634887-mqdaAEVY",
            "production" => "1654946446-zaaXD37a",
        ],
        "registerSticker" => [
            "dev" => "1654946446-mdyGQNwy",
            "production" => "1654946446-mdyGQNwy",
        ],
        "payment" => [
            "dev" => "1657634887-L6nrBeAR",
            "production" => "1654946446-DvREp12R",
        ],
        "loginStaff" => [
            "dev" => "1654946446-5VMYAV3M",
            "production" => "1654946446-5VMYAV3M",
        ],
        "homeSetting" => [
            "dev" => "1657634887-y6REqYQ1",
            "production" => "1654946446-rwq63ynq",
        ],
        "reportHome" => [
            "dev" => "1654652226-ZAp2aYkR",
            "production" => "1654946446-aozDNRvz",
        ],
        "eBill" => [
            "dev" => "1654946446-qzXNzEVX",
            "production" => "1654946446-qzXNzEVX",
        ],
        "eBillTest" => [
            "dev" => "1654946446-yAaR6vGa",
            "production" => "1654946446-yAaR6vGa",
        ],
        "userDelete" => [
            "dev" => "1654652226-v7L5zVQM",
            "production" => "1654652226-v7L5zVQM",
        ],
        "paymentTest" => [
            "dev" => "1654946446-EaWXmQrW",
            "production" => "1654946446-EaWXmQrW",
        ],
        "verify" => [
            "dev" => "1657634887-xzPQly9m",
            "production" => "1654946446-rjyKB52y",
        ],

    ];

    public function __get($varName)
    {
        $status = env('LIFF_ENV', 'dev');

        if (!array_key_exists($varName, $this->data)) {
            //this attribute is not defined!

        } else {
            return [
                "key" => $this->data[$varName][$status],
                "url" => "{$this->baseUrl}{$this->data[$varName][$status]}",
            ];
        }
    }

    public function all()
    {
        $status = env('LIFF_ENV', 'dev');

        $datas = [];
        foreach ($this->data as $index => $data) {
            array_push($datas, [
                "key" => $data[$status],
                "url" => "{$this->baseUrl}{$data[$status]}",
                "name" => $index,
            ]);
        }

        return $datas;
    }

    public function get($name, $query = false)
    {
        $query = (array) $query;
        $status = env('LIFF_ENV', 'dev');
        $q = http_build_query($query);
        return "{$this->baseUrl}{$this->data[$name][$status]}?{$q}";
    }
}

function liffAll()
{
    $liff = new LineLiff();

    return $liff->all();
}
