<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Home extends Model
{
    // use HasHashid, HashidRouting;

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

    protected $primaryKey = 'id';
    protected $appends = ['created_at_text'];

    protected $fillable = ['ca', 'ui', 'name', 'user_id', 'request_e_document_at', 'uuid', 'ebill_email'];
    protected $hidden = ['user_id', 'updated_at'];

    public function scopeFindByHashid($query, $uid)
    {
        return $query->where('uuid', $uid)->first();
    }

    public function getIdAttribute()
    {
        return $this->uuid;
    }

    public function getCreatedAtTextAttribute()
    {
        return $this->created_at ? Carbon::parse($this->created_at)->format('d/m/Y H:i:s') : '-';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
