<?php

namespace App\Http\Controllers;

use App\Home;
use Illuminate\Http\Request;
use Carbon\Carbon;

class EdocumentController extends Controller
{
    public function requestEdoc($ui, Request $request)
    {
        $home = Home::findByHashid($ui);

        if ($home->request_e_document_at) {
            $home->update([
                "request_e_document_at" => null
            ]);
        } else {
            $home->update([
                "request_e_document_at" => Carbon::now()
            ]);
        }

        return 'success';
    }
}
