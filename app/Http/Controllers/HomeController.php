<?php

namespace App\Http\Controllers;

use App\Home;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
// use Mtownsend\XmlToArray\XmlToArray;
use Illuminate\Support\Arr;
use SimpleSoftwareIO\QrCode\Generator;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $keyword = $request->q;
        $houses = Home::where(function ($q) use ($keyword) {
            $q->orWhere('ca', 'LIKE', "%$keyword%")
                ->orWhere('ui', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%");
        })->paginate(10);
        $count = Home::count();

        return [
            "houses" => $houses,
            "count" => $count,
        ];
    }

    public function homes(Request $request)
    {
        return Home::with('user')->get();
    }
    public function formatDateThat($strDate)
    {
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));
        $strMonthCut = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $strMonthThai = $strMonthCut[$strMonth];
        return ['m' => $strMonthThai, 'y' => $strYear, 'd' => $strDay];
    }

    public function qrGen()
    {
        $qrcode = new Generator;

        $home = Home::findByHashid(request()->id);
        $homeArr = explode('|', $this->loadHomeMea($home->ca));
        $kwh = str_pad($homeArr[23], 8, '0', STR_PAD_LEFT);
        $price = str_replace('.', '', $homeArr[14]);

        $y = substr($homeArr[24], 0, 4) + 543;
        $m = substr($homeArr[24], 4, 2);
        $d = substr($homeArr[24], 6, 8);
        $y = substr($y, 4 - 2);
        $thdate = ("{$d}{$m}{$y}");

        $qr = "|099400016520011
    {$home['ca']}{$kwh}1
    {$homeArr[21]}{$thdate}
    {$price}
    ";

        // return ['e' => $qr, 'home' => $homeArr[24]];

        $qrcode->size(500)->style('round', 0.5)->generate($qr);

        return $qrcode;
        // return  QRCode::text($qr)->png();
    }
    public function checkValid(Request $request)
    {
        $curl = curl_init();

        $ca = $request->ca;
        $ui = $request->ui;

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('ESERVICE_API_URL') . '/E-SERVICE/MEA_VALIDATE_CUST?ca=CA' . $ca . '&ui=UI' . $ui,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        // $array = XmlToArray::convert($response);

        $xml = simplexml_load_string($response);
        $json = json_encode($xml);

        $array = json_decode($json, true);

        if (!$response) {
            return ["message" => "มีบางอย่างผิดพลาดกรุณาลองใหม่อีกครั้ง", "status" => 0];
        }

        return response()->json([
            "message" => 'ok',
            "status" => $array['Status'],
            // "a" => 'http://10.211.70.43:18972/E-SERVICE/MEA_VALIDATE_CUST?ca=CA' . $ca . '&ui=UI' . $ui,
        ]);
    }

    private function loadHomeMea($ca)
    {
        $request = request();
        if ($request->test) {
            return $request->test;
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('ESERVICE_API_URL') . "/PAYGATEWAY_V2/MEAOPS_QUERY?req=MEACA" . $ca . "MEALNTH",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }
    public function LoadHome($ui, Request $request)
    {

        $home = Home::findByHashid($ui);
        // $home = Home::find($ui);
        if (!$home) {
            return '';
        }
        $user = $home->user;

        $homeArr = explode('|', $this->loadHomeMea($home->ca));

        $homeDetail = [];
        if (count($homeArr) > 21) {
            $nobill = false;
            $homeDetail = [
                'amount' => $homeArr[14] * 1,
                'due_date' => $homeArr[24],
                'last_date' => $homeArr[22],
                'kwh' => $homeArr[23],
                'invoice_no' => $homeArr[21],
                'installation' => $homeArr[2],
                'ca' => $homeArr[3],
                'ui' => $homeArr[2],

                'dept_code' => $homeArr[19],
                'DiscNoVat' => $homeArr[12],
                'DiscVatAmt' => $homeArr[13],
                'TotalVat' => $homeArr[15],
                'IntDay' => $homeArr[29],
                'IntAmt' => $homeArr[30],

                'TotalBill' => $homeArr[10],
                'TotAmt' => $homeArr[27],
                'DisconnetAmt' => $homeArr[11],
                'TotalInt' => $homeArr[16],
                'TransNo' => $homeArr[1],

            ];
        } else {
            $nobill = true;
        }

        $homeDetail['pay_flag'] = [
            'status' => $homeArr[17],
            'message' => $homeArr[18],
        ];

        if ($homeArr[17] == 'N') {
            $nobill = true;
        }

        $request = request();
        if ($request->test) {
            $home['ca'] = $homeDetail['ca'];
            $home['ui'] = $homeDetail['ui'];
            $home->save();
        }

        return response()->json([

            'customer' => [
                'name' => $homeArr[4],
                'address' => $homeArr[5],
                'tel' => $user->tel,
            ],
            'home' => $home,
            'nobil' => $nobill,
            'home_detail' => $homeDetail,
        ]);
    }
    // public function index()
    // {
    //     $homes =  Home::with('user')->orderBy('user_id')->orderBy('ca')->orderBy('ui')->get();
    // }

    public function history($ui)
    {
        $curl = curl_init();

        $today = Carbon::now()->format('Ymd');
        $oldday = Carbon::now()->subMonths(7)->format('Ymd');
        $home = Home::findByHashid($ui);

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('ESERVICE_API_URL') . "/E-SERVICE/InvHistJSON?ui={$home->ui}&startdate={$oldday}&enddate={$today}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode(
            $response,
            true
        );

        if (!$response) {
            return ["message" => "มีบางอย่างผิดพลาดกรุณาลองใหม่อีกครั้ง", "url" => env('ESERVICE_API_URL') . "/E-SERVICE/InvHistJSON?ui={$home->ui}&startdate={$oldday}&enddate={$today}"];
        }

        $result = [];

        foreach ($response['invoices'] as $invoice) {
            $filter = Arr::only($invoice, ['nettotal', 'discavat_amt', 'invoice_no', 'print_date', 'kwh', 'bill_period', 'calculate_date', 'meter_type', 'kwhon', 'kwhof']);
            $filter['price'] = round($filter['nettotal'] - $filter['discavat_amt'], 2);
            $date = $this->formatDateThat(Carbon::parse($invoice['schdate']));
            // $filter['date_text'] = "{$date['m']} {$date['y']}";
            $filter['month_name'] = "{$date['m']}";
            $filter['detail']['date_text'] = "{$date['m']} {$date['y']}";

            $calDate = $this->formatDateThat(Carbon::parse($invoice['calculate_date']));

            $filter['calculate_date_text'] = "{$calDate['d']} {$calDate['m']} {$calDate['y']}";

            $filter['detail']['description'] = "";

            if ($filter['meter_type'] == 'TOU') {
                $filter['kwh'] = $filter['kwhon'] + $filter['kwhof'];
            }

            if (count($result) <= 5) {
                array_push($result, $filter);
            }
        }
        return ['message' => 'ok', 'result' => $result, "url" => env('ESERVICE_API_URL') . "/E-SERVICE/InvHistJSON?ui={$home->ui}&startdate={$oldday}&enddate={$today}"];
    }

    public function store(Request $request)
    {

        $user = User::where('line_user_id', $request->user_id)->first();

        if (!$user) {
            return [
                'status' => 400,
                "message" => 'ไม่พบผู้ใช้งาน',
            ];
        }

        $checkInvalid = Home::where([['ca', $request->ca], ['ui', $request->ui], ['user_id', $user->id]])->first();

        if ($checkInvalid) {
            return [
                'status' => 400,
                "message" => 'ผู้ใช้งานทำการลงทะเบียนบ้านหลังนี้แล้ว',
            ];
        }

        $checkMax = Home::where([['user_id', $user->id]])->count();

        if ($checkMax >= 10) {
            return [
                'status' => 400,
                "message" => 'สามารถเพิ่มบ้านได้สูงสุด 9 รายการ',
            ];
        }

        $req = $request->all();
        $user = User::where('line_user_id', $request->user_id)->first();
        $req['user_id'] = $user->id;

        $home = Home::create($req);

        return [
            'status' => 200,
            "message" => 'create home success',
        ];
    }

    public function destroy($ui)
    {
        $home = Home::findByHashid($ui);
        $home->delete();
        return 'delete home success';
    }

    public function checkUserBlocked($line_user_id)
    {

        $curl
            = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.line.me/v2/bot/profile/{$line_user_id}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer oRHk3Pt39ACQZrFiojNSnB6ZJqsFbtEopW5Gt43ILhLfB9dCf61T++fitgYYMY18XLVc2Ym9jMpwgu+l8xnDuSjmsTdE3bdx8WBcVcEHkxQ+4pSoBBIQQNCR6uIAOZchC0pVFpGt5ZOH8uT93bVViQdB04t89/1O/w1cDnyilFU=",
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode(
            $response,
            true
        );

        curl_close($curl);

        $status = false;
        if (isset($response['message'])) {
            if ($response['message'] == 'Not found') {
                $status = true;
            }
        }
        return $status;
    }
    public function getLineUserIdByUI(Request $request)
    {

        $results = [];
        $cas = Home::whereIn('ui', $request->ui)->get()->groupBy('ui');

        foreach ($cas as $key => $ui) {
            $items = [
                "ui" => $key,
                "users" => [],
            ];
            foreach ($ui as $home) {

                $user = $home->user;
                if ($user) {
                    array_push($items['users'], [
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'email' => $user->email,
                        'line_user_id' => $user->line_user_id,
                        'user_block' => $this->checkUserBlocked($user->line_user_id),
                    ]);
                }
            }
            array_push($results, $items);
        }

        return [
            "result" =>
            $results,
        ];
    }
}
