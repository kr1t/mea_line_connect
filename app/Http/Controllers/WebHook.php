<?php

namespace App\Http\Controllers;

use App\Config;
use App\Home;
use App\Log;
use App\PushMessage;
use App\Services\PayUService\Exception;
use App\User;
use Carbon\Carbon;
use Helpers\LineBot;
use Helpers\LineLiff;
use Illuminate\Http\Request;

class WebHook extends Controller
{

    private $liff;
    private $bot;

    public function __construct()
    {
        $this->liff = new LineLiff();
        $this->bot = new LineBot(env('LINE_MESSAGING_TOKEN'));
    }
    public function index(Request $request)
    {

        try {

            $REPLY_MSG_NEED_AUTH = [
                'PAYMENT' => ['ชำระค่าไฟ', 'ค่าไฟ', 'จ่ายเงินค่าไฟ', 'จ่ายบิล', 'ชำระเงิน', 'เช็คยอดค่าไฟ'],
                'PAYMENT_TEST' => ['ชำระค่าไฟ-ทดสอบ'],
                'REPORT' => ['แจ้งไฟฟ้าขัดข้อง', 'ไฟดับ', 'ไฟฟ้าขัดข้อง', 'หม้อแปลงระเบิด', 'เสาไฟล้ม', 'ชนเสาไฟ', 'ไฟช็อต', 'แจ้งเหตุ'],
                'E_DOCUMENT' => ['รับเอกสาร', 'บิลค่าไฟ', 'บิลออนไลน์', 'ใบแจ้งค่าไฟ', 'ขอบิล', 'ออนไลน์', 'E-Bill'],
                'SETTING' => ['จัดการข้อมูล', 'แก้ไขข้อมูล', 'เพิ่มข้อมูล', 'ตั้งค่า', 'จัดการ', 'แก้ไข'],
                'SETTING_HOME' => ['จัดการข้อมูลบ้าน', 'แก้ชื่อ', 'แก้เบอร์', 'อีเมล', 'email', 'e mail'],
                'SETTING_PROFILE' => ['จัดการข้อมูลโปรไฟล์', 'เพิ่มบ้าน', 'ลบบ้าน', 'ข้อมูลบ้าน', 'แก้บ้านเลขที่'],
                'SETTING_OTHER' => ['อื่นๆ', 'พนักงาน', 'เจ้าของ', 'กิจการ', 'ธุรกิจ'],
                'CONTACT_ME' => ['ติดต่อ', 'ติดต่อเรา'],
                'DELUSER' => ['#ลบบัญชี'],
                'E_DOCUMENT_TEST' => ['#ทดสอบ_eBill'],
            ];
            $REPLY_MSG_NO_AUTH = [];


            if ($request['events']) {
                if (sizeof($request['events']) > 0) {
                    foreach ($request['events'] as $event) {
                        $reply_token = $event['replyToken'];
                        $text = isset($event['message']['text']) ? $event['message']['text'] : '';
                        $userID = $event['source']['userId'];

                        $this->bot->setReplyToken($reply_token);
                        $this->bot->setUser($userID);


                        if ($text == '#ทดสอบ') {
                            return $this->bot->addText($userID)->reply();
                        }
                        //                         if (
                        //                             $userID != 'U82740f54e80207cfa39de21970632775'
                        //                             && $userID != 'U3edb511ed4ef4b083485cd07dc63f400'
                        //                             && $userID != 'U4541add693d6bff1c2daa0c372c82549'
                        //                             && $userID != 'U3d35f9492e7e5c289a681a0a93dc3566'

                        //                         ) {
                        //                             return $this->bot->addText('🙏ขออภัยในความไม่สะดวก เพื่อยกระดับการให้บริการ MEA Connect ขอปรับปรุงระบบชั่วคราว ในวันเสาร์ที่ 13 ก.พ. 2564 เวลา 07.00-10.00 น.

                        // ** ทั้งนี้จะไม่ส่งผลกระทบกับระบบไฟฟ้า และงานบริการด้านไฟฟ้า **

                        // สอบถามข้อมูลได้ทาง Social Media หรือ MEA Call Center 1130 ตลอด 24 ชั่วโมง

                        // #พลังงานเพื่อวิถีชีวิตเมืองมหานคร
                        // Energy for city life, Energize smart living')->reply();
                        //                         }



                        $checkRegister = $this->checkRegister($userID);

                        if ($checkRegister != true) {
                            return $this->sendRegisterImage($reply_token);
                        }


                        // LOGIC FROM REPLY MESSAGE //
                        // Log::create([
                        //     "message" => 'after auth'
                        // ]);

                        $check = function ($name) use ($text, $REPLY_MSG_NEED_AUTH) {
                            $checkMenu = false;
                            foreach ($REPLY_MSG_NEED_AUTH[$name] as $menu) {
                                if ($menu == $text) {
                                    $checkMenu = true;
                                }
                            }
                            return $checkMenu;
                        };

                        $user = $this->bot->getUser();

                        if (!$user->verify) {
                            return $this->bot->addBubble('กรุณายืนยัน OTP', ($this->generatePleaseOTPCard()))->reply();
                        }

                        if ($text == 'ติดต่อ#แจ้งเหตุขัดข้อง') {
                            return $this->openCase(1);
                        } else if ($text == 'ติดต่อ#ร้องเรียน') {
                            return $this->openCase(2);
                        } else if ($text == 'ติดต่อ#สอบถามข้อมูล') {
                            return $this->openCase(3);
                        } else if ($text == 'ติดต่อ#เสนอแนะ') {
                            return $this->openCase(4);
                        } else if ($text == 'ติดต่อ#บริการอื่นๆ') {
                            return $this->openCase(5);
                        } else if ($check('PAYMENT')) {
                            return $this->showPaymentList($reply_token, $userID);
                        } else if ($check('PAYMENT_TEST')) {
                            $this->showPaymentListCovid($reply_token, $userID);
                        } else if ($check('REPORT')) {
                            $this->showReportList($reply_token, $userID);
                        } else if ($check('E_DOCUMENT')) {
                            $this->showEDocList($reply_token, $userID);
                        } else if ($check('E_DOCUMENT_TEST')) {
                            $this->showEDocListTest($reply_token, $userID);
                        } else if ($check('SETTING')) {
                            $this->showSettingList($reply_token, $userID);
                        } else if ($check('SETTING_HOME')) {
                            $this->showSettingHomeList($reply_token, $userID);
                        } else if ($check('SETTING_PROFILE')) {
                            return $this->showSettingProfileList($reply_token, $userID);
                        } else if ($check('SETTING_OTHER')) {
                            $this->showSettingOtherList($reply_token, $userID);
                        } else if ($check('CONTACT_ME')) {
                            $this->sendOpenChatWarroom();
                        } else if ($check('DELUSER')) {
                            $user = User::where('line_user_id', $userID)->first();
                            // $home = Home::where('user_id', $user->id)->delete();
                            $user->delete();
                            return 'ok';
                        } else if ($user->expiry_callcenter && $user->expiry_callcenter > Carbon::now()) {
                            return $this->sendOpenChatWarroom();
                        } else {

                            $time = Carbon::now();
                            if ($user->send_to_warroom_datetime && $user->send_to_warroom_datetime > $time) {
                                return $this->sendWarroom($request->all());
                            }
                            $this->bot->addText('กรุณากดรายการที่ท่านต้องการใช้บริการได้จากเมนูหลัก')->reply();
                        }


                        return [
                            'status' => 200,
                        ];

                        // END LOGIC FROM REPLY MESSAGE //

                    }
                }
            }

            return 'NOBOT';
        } catch (\Exception $e) {
            // Log::create([
            //     "message" =>
            //     $e->getMessage(),
            // ]);

            return
                $e->getMessage();
        }

        // Failed

    }

    public function verifyCard()
    {
        return [
            "type" => "bubble",
            "size" => "mega",
            "header" => [
                "type" => "box",
                "layout" => "vertical",
                "contents" => [
                    [
                        "type" => "image",
                        "url" => "https://i.imgur.com/FQ1vRsO.png",
                        "size" => "3xl"
                    ],
                    [
                        "type" => "box",
                        "layout" => "vertical",
                        "contents" => [
                            [
                                "type" => "text",
                                "text" => "ตรวจสอบเครื่องวัดไฟฟ้า",
                                "color" => "#ffffff",
                                "size" => "xl",
                                "flex" => 4,
                                "weight" => "bold",
                                "align" => "center"
                            ]
                        ]
                    ],
                    [
                        "type" => "box",
                        "layout" => "vertical",
                        "contents" => [
                            [
                                "type" => "text",
                                "text" => "ที่มีความเป็นเจ้าของ",
                                "color" => "#ffffff",
                                "size" => "lg",
                                "flex" => 4,
                                "weight" => "regular",
                                "align" => "center"
                            ]
                        ]
                    ]
                ],
                "paddingAll" => "20px",
                "backgroundColor" => "#f67e2c",
                "spacing" => "md",
                "height" => "350px",
                "paddingTop" => "22px",
                "background" => [
                    "type" => "linearGradient",
                    "angle" => "120deg",
                    "startColor" => "#f67e2c",
                    "endColor" => "#E36D1C",
                    "centerColor" => "#f78536"
                ],
                "action" => [
                    "type" => "uri",
                    "label" => "action",
                    "uri" => $this->liff->get('verify')
                ]
            ],
            "footer" => [
                "type" => "box",
                "layout" => "vertical",
                "contents" => []
            ]
        ];
    }
    public function openCase($case_id)
    {
        $user = $this->bot->getUser();

        $user->update([
            'callcenter_case' => $case_id,
            'expiry_callcenter' => null,
        ]);

        $CALLCENTER_CASE = [
            ['id' => 1, "label" => "แจ้งเหตุขัดข้อง"],
            ['id' => 2, "label" => "ร้องเรียน"],
            ['id' => 3, "label" => "สอบถามข้อมูล"],
            ['id' => 4, "label" => "เสนอแนะ"],
            ['id' => 5, "label" => "อื่นๆ"],
        ];

        $case = $CALLCENTER_CASE[$case_id - 1]['label'];
        $name = $user->first_name . " " . $user->last_name;
        $text = "เรื่อง: {$case}\nName: {$name}\nEmail: {$user->email}\nTel: {$user->tel}\n{$user->uis}";

        // $this->bot->addText($text)->push();
        $req = request()->all();

        $req['events'][0]['message']['text'] = $text;
        $this->sendWarroom($req);

        return $this->bot->addText('สวัสดีค่ะ MEA ยินดีให้บริการ กรุณาพิมพ์ข้อความเพื่อแจ้งเจ้าหน้าที่')
            // ->addText("(Log เพื่อทดสอบ)\n" . $text)
            ->reply();
    }
    public function sendOpenChatWarroom()
    {

        $user = $this->bot->getUser();
        // minutes 30
        $user->update([
            'send_to_warroom' => 0,
            'expiry_callcenter' => Carbon::now()->addMinutes(30),
        ]);

        return $this->bot->addText('กรุณาเลือกรายการที่ต้องการติดต่อ 1 รายการ')
            ->addCarousel('กรุณาเลือกรายการที่ต้องการติดต่อ 1 รายการ', [
                [
                    "type" => "bubble",
                    "hero" => [
                        "type" => "image",
                        "url" => ("https://i.imgur.com/PKOklJo.jpg"),
                        "size" => "full",
                        "aspectRatio" => "20:13",
                        "aspectMode" => "cover",

                    ],
                    "body" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "spacing" => "md",

                        "contents" => [
                            [
                                "type" => "text",
                                "text" => "แจ้งเหตุขัดข้อง",
                                "weight" => "bold",
                                "size" => "xl",

                            ],
                            [
                                "type" => "text",
                                "text" => "ไฟฟ้าขัดข้อง/อุปกรณ์ไฟฟ้าชำรุด/สายสื่อสาร",
                                "size" => "sm",
                                "color" => "#AAAAAA",
                                "wrap" => true,

                            ],
                        ],
                    ],
                    "footer" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "contents" => [
                            [
                                "type" => "button",
                                "action" => [
                                    "type" => "message",
                                    "label" => "ติดต่อ",
                                    "text" => "ติดต่อ#แจ้งเหตุขัดข้อง",
                                ],
                                "color" => "#F37022",
                                "style" => "primary",
                            ],
                        ],
                    ],
                ],
                //~แจ้งเหตุขัดข้อง
                [
                    "type" => "bubble",
                    "hero" => [
                        "type" => "image",
                        "url" => url("/images/BIURmTf.jpg"),
                        "size" => "full",
                        "aspectRatio" => "20:13",
                        "aspectMode" => "cover",

                    ],
                    "body" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "spacing" => "md",

                        "contents" => [
                            [
                                "type" => "text",
                                "text" => "สอบถามข้อมูล",
                                "weight" => "bold",
                                "size" => "xl",

                            ],
                            [
                                "type" => "text",
                                "text" => "ค่าไฟฟ้า/งานบริการ/สมัครงาน/กิจกรรมของ MEA",
                                "size" => "sm",
                                "color" => "#AAAAAA",
                                "wrap" => true,

                            ],
                        ],
                    ],
                    "footer" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "contents" => [
                            [
                                "type" => "button",
                                "action" => [
                                    "type" => "message",
                                    "label" => "ติดต่อ",
                                    "text" => "ติดต่อ#สอบถามข้อมูล",
                                ],
                                "color" => "#F37022",
                                "style" => "primary",
                            ],
                        ],
                    ],
                ],
                //~สอบถามข้อมูล
                [
                    "type" => "bubble",
                    "hero" => [
                        "type" => "image",
                        "url" => url("/images/5Z5AAde.jpg"),
                        "size" => "full",
                        "aspectRatio" => "20:13",
                        "aspectMode" => "cover",

                    ],
                    "body" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "spacing" => "md",

                        "contents" => [
                            [
                                "type" => "text",
                                "text" => "แนะนำ-ติ ชม",
                                "weight" => "bold",
                                "size" => "xl",

                            ],
                            [
                                "type" => "text",
                                "text" => "การบริการ/การใช้ Application/กิจกรรมต่างๆ",
                                "size" => "sm",
                                "color" => "#AAAAAA",
                                "wrap" => true,

                            ],
                        ],
                    ],
                    "footer" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "contents" => [
                            [
                                "type" => "button",
                                "action" => [
                                    "type" => "message",
                                    "label" => "ติดต่อ",
                                    "text" => "ติดต่อ#เสนอแนะ",
                                ],
                                "color" => "#F37022",
                                "style" => "primary",
                            ],
                        ],
                    ],
                ],
                //~แนะนำ-ติชม
                [
                    "type" => "bubble",
                    "hero" => [
                        "type" => "image",
                        "url" => url("/images/IKj42ey.jpg"),
                        "size" => "full",
                        "aspectRatio" => "20:13",
                        "aspectMode" => "cover",

                    ],
                    "body" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "spacing" => "md",

                        "contents" => [
                            [
                                "type" => "text",
                                "text" => "ร้องเรียน",
                                "weight" => "bold",
                                "size" => "xl",

                            ],
                            [
                                "type" => "text",
                                "text" => "การให้บริการของ MEA",
                                "size" => "sm",
                                "color" => "#AAAAAA",
                                "wrap" => true,

                            ],
                        ],
                    ],
                    "footer" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "contents" => [
                            [
                                "type" => "button",
                                "action" => [
                                    "type" => "message",
                                    "label" => "ติดต่อ",
                                    "text" => "ติดต่อ#ร้องเรียน",
                                ],
                                "color" => "#F37022",
                                "style" => "primary",
                            ],
                        ],
                    ],
                ],
                //~ร้องเรียน

                [
                    "type" => "bubble",
                    "hero" => [
                        "type" => "image",
                        "url" => url("/images/5BuzobD.jpg"),
                        "size" => "full",
                        "aspectRatio" => "20:13",
                        "aspectMode" => "cover",

                    ],
                    "body" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "spacing" => "md",

                        "contents" => [
                            [
                                "type" => "text",
                                "text" => "สอบถามบริการอื่นๆ",
                                "weight" => "bold",
                                "size" => "xl",

                            ],
                            // [
                            //     "type" => "text",
                            //     "text" => "ม็อบ บอดี้แรงผลัก พรีเซ็นเตอร์เฮอร์ริเคนป๊อก ",
                            //     "size" => "sm",
                            //     "color" => "#AAAAAA",
                            //     "wrap" => true,

                            // ],
                        ],
                    ],
                    "footer" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "contents" => [
                            [
                                "type" => "button",
                                "action" => [
                                    "type" => "message",
                                    "label" => "ติดต่อ",
                                    "text" => "ติดต่อ#บริการอื่นๆ",
                                ],
                                "color" => "#F37022",
                                "style" => "primary",
                            ],
                        ],
                    ],
                ],
            ])->reply();
    }

    public function sendCloseChatWarroom(Request $request)
    {


        $req = $request->all();
        if (!isset($req['identity']['warroomThreadIdentityID'])) {
            return response()->json([
                "message" => "invalid [identity.warroomThreadIdentityID]",
            ], 400);
        }
        $user_id = explode('-', $req['identity']['warroomThreadIdentityID'])[1];

        $user = User::whereLineUserId($user_id)->first();

        if (!$user) {

            Log::create([
                'message' => 'User Not Found.',
            ]);

            return response()->json([
                "message" => "User Not Found.",
            ], 400);
        }

        Log::create([
            'message' => 'CloseWarroom Start: ' . $user->first_name,
        ]);

        $user->update([
            'send_to_warroom_datetime' => null,
        ]);

        $m = [
            [
                "type" => "flex",
                "altText" => 'หากต้องการติดต่อสอบถาม กรุณากด"ติดต่อ Call Center"อีกครั้ง',
                "contents" => array(
                    'type' => 'carousel',
                    'contents' => [[
                        "type" => "bubble",
                        "hero" => [
                            "type" => "image",
                            "url" => url('images/callcenter_thank.jpg'),
                            "size" => "full",
                            "aspectRatio" => "20:13",
                            "aspectMode" => "cover",
                        ],
                        "body" => [
                            "type" => "box",
                            "layout" => "vertical",
                            "contents" => [
                                [
                                    "type" => "text",
                                    "text" => "ขอบคุณที่ติดต่อ MEA",
                                    "weight" => "bold",
                                    "size" => "xl",
                                ],
                                [
                                    "type" => "text",
                                    "text" => "หากต้องการติดต่อสอบถาม",
                                    "margin" => "lg",
                                ],
                                [
                                    "type" => "text",
                                    "text" => 'กรุณากด"ติดต่อ Call Center"อีกครั้ง',
                                ],
                            ],
                        ],
                        "footer" => [
                            "type" => "box",
                            "layout" => "vertical",
                            "spacing" => "sm",
                            "contents" => [
                                [
                                    "type" => "button",
                                    "style" => "link",
                                    "height" => "sm",
                                    "action" => [
                                        "type" => "message",
                                        "label" => "Call Center",
                                        "text" => "ติดต่อเรา",
                                    ],
                                ],
                                [
                                    "type" => "spacer",
                                    "size" => "sm",
                                ],
                            ],
                            "flex" => 0,
                        ],
                    ]],

                ),
            ],
        ];
        $data = [
            'to' => $user_id,
            'messages' => $m,
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);
        $send_result = $this->send_push_message($post_body);
        Log::create([
            'message' => 'CloseWarroom End: ' . $user->first_name,
            'post_body' => json_encode($send_result)
        ]);

        return $send_result;
    }

    public function sendWarroom($req, $addTime = true)
    {

        $user = $this->bot->getUser();

        if ($addTime) {
            $user->update([
                'send_to_warroom_datetime' => Carbon::now()->addMinutes(30)
            ]);
        }


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('WARROOM_API'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($req),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
    public function sendRegisterImage($reply_token)
    {
        return $this->bot->addText('กรุณาทำการลงทะเบียนก่อนใช้งานฟังชันดังกล่าว')->addImageURI(
            'กรุณาทำการลงทะเบียนก่อนใช้งานฟังชันดังกล่าว',
            'https://i.imgur.com/LbgKPOn.jpg',
            // 'https://i.imgur.com/zYe0X71.png',
            $this->liff->get('register'),
            1040,
            1040
        )->reply();
    }
    public function generatePleaseOTPCard()
    {
        $url = $this->liff->get('register');
        return array(
            'type' => 'bubble',
            'hero' => array(
                'type' => 'image',
                'url' => url('assets/images/please_add_home.jpg'),
                'size' => 'full',
                'aspectRatio' => '20:13',
                'aspectMode' => 'cover',
                'action' => array(
                    'type' => 'uri',
                    'uri' => $url,
                ),
            ),
            'body' => array(
                'type' => 'box',
                'layout' => 'vertical',
                'spacing' => 'md',
                'action' => array(
                    'type' => 'uri',
                    'uri' => $url,
                ),
                'contents' => array(
                    0 => array(
                        'type' => 'text',
                        'text' => 'การยืนยันบัญชีของคุณยังไม่สำเร็จ',
                        'size' => 'md',
                        'weight' => 'bold',
                        'align' => 'center',
                    ),
                    1 => array(
                        'type' => 'text',
                        'text' => 'กรุณายืนยันรหัสผ่าน OTP',
                        'wrap' => true,
                        'color' => '#aaaaaa',
                        'size' => 'sm',
                        'align' => 'center',
                    ),
                ),
            ),
            'footer' => array(
                'type' => 'box',
                'layout' => 'vertical',
                'contents' => array(
                    0 => array(
                        'type' => 'spacer',
                        'size' => 'xs',
                    ),
                    1 => array(
                        'type' => 'button',
                        'style' => 'primary',
                        'color' => '#f37022',
                        'action' => array(
                            'type' => 'uri',
                            'label' => 'เปิดหน้ายืนยัน OTP',
                            'uri' => $url,
                        ),
                    ),
                ),
            ),
        );
    }
    public function generatePleaseAddHomeCard($no_home = 'payment')
    {
        $url = $this->liff->get('addHome', ["no_home" => $no_home]);
        return array(
            'type' => 'bubble',
            'hero' => array(
                'type' => 'image',
                'url' => url('assets/images/please_add_home.jpg'),
                'size' => 'full',
                'aspectRatio' => '20:13',
                'aspectMode' => 'cover',
                'action' => array(
                    'type' => 'uri',
                    'uri' => $url,
                ),
            ),
            'body' => array(
                'type' => 'box',
                'layout' => 'vertical',
                'spacing' => 'md',
                'action' => array(
                    'type' => 'uri',
                    'uri' => $url,
                ),
                'contents' => array(
                    0 => array(
                        'type' => 'text',
                        'text' => 'ไม่พบรายการบ้านในระบบ',
                        'size' => 'xl',
                        'weight' => 'bold',
                        'align' => 'center',
                    ),
                    1 => array(
                        'type' => 'text',
                        'text' => 'กรุณาลงทะเบียนเพื่อใช้งานระบบดังกล่าว',
                        'wrap' => true,
                        'color' => '#aaaaaa',
                        'size' => 'sm',
                        'align' => 'center',
                    ),
                ),
            ),
            'footer' => array(
                'type' => 'box',
                'layout' => 'vertical',
                'contents' => array(
                    0 => array(
                        'type' => 'spacer',
                        'size' => 'xxl',
                    ),
                    1 => array(
                        'type' => 'button',
                        'style' => 'primary',
                        'color' => '#f37022',
                        'action' => array(
                            'type' => 'uri',
                            'label' => 'ลงทะเบียนบ้าน',
                            'uri' => $url,
                        ),
                    ),
                ),
            ),
        );
    }
    public function sendPleaseAddHome($reply_token)
    {
        $card = $this->generatePleaseAddHomeCard();
        $m = [
            [
                "type" => "flex",
                "altText" => "กรุณาลงทะเบียนบ้านอย่างน้อย 1 รายการเพื่อทำรายการดังกล่าว",
                "contents" => [
                    "type" => "carousel",
                    "contents" => [$card, $this->verifyCard()],
                ],
            ],
        ];
        $data = [
            'replyToken' => $reply_token,
            'messages' => $m,
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        $send_result = $this->send_reply_message($post_body);

        // Log::create([
        //     'message' => 'Result: ' . $send_result . '\r\n',
        //     'reply_token' => $reply_token,
        //     'post_body' => $post_body,
        // ]);
        return $send_result;
    }

    public function test()
    {


        $checkMax = Home::where([['user_id', 2]])->count();


        return $checkMax;
        $this->bot->setUser('U2d9ef70b54f10f49329660b214bc1196');

        return $this->bot->addText('กรุณาทำการลงทะเบียนก่อนใช้งานฟังชันดังกล่าว')->addImageURI(
            'กรุณาทำการลงทะเบียนก่อนใช้งานฟังชันดังกล่าว',
            url('images/LbgKPOn.jpg'),
            $this->liff->get('register')
        )->push();

        // return $this->sendOpenChatWarroom('a', 'wdq');
        // return $this->sendPleaseAddHome('a', 'U2d9ef70b54f10f49329660b214bc1196');
        // return $this->showPaymentList('a', 'U2d9ef70b54f10f49329660b214bc1196');
    }

    public function loadStaffCard($id)
    {
        return $this->showStaffList($id);
    }
    public function showPaymentList($reply_token, $user_id)
    {

        $cards = [];

        $user = User::where('line_user_id', $user_id)->first();

        $homes = Home::where('user_id', $user->id)->get();

        if (!count($homes)) {
            return $this->sendPleaseAddHome($reply_token, 'payment');
        }

        foreach ($homes as $home) {

            $liffUrl = $this->liff->get('payment', ["meter_no" => $home->id]);
            array_push($cards, ['title' => $home->name, 'description' => 'ดูรายละเอียดค่าไฟฟ้าของคุณ', 'url' => $liffUrl]);
        }

        $generateCard = $this->generateCard(
            url('images/payment.png'),
            'ตรวจสอบยอดค้างชำระ / ส่วนลดค่าไฟ',
            $cards
        );

        array_push($generateCard, $this->verifyCard());

        $m = [
            [
                "type" => "flex",
                "altText" => "เช็คยอดค้าง และ ชำระค่าไฟฟ้าได้ที่นี่",
                "contents" => array(
                    'type' => 'carousel',
                    'contents' => $generateCard,

                ),
            ],
        ];

        $data = [
            'replyToken' => $reply_token,
            'messages' => $m,
            'to' => $user->line_user_id
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        // Log::create([
        //     "message" => 'add json payments',
        //     "post_body" => $post_body
        // ]);

        $send_result = $this->send_reply_message($post_body);

        // Log::create([
        //     'message' => 'Result: ' . $send_result . '\r\n',
        //     'reply_token' => $reply_token,
        //     'post_body' => $post_body,
        // ]);

        return $send_result;
    }


    public function showPaymentListCovid($reply_token, $user_id)
    {

        $cards = [];

        $user = User::where('line_user_id', $user_id)->first();

        $homes = Home::where('user_id', $user->id)->get();

        if (!count($homes)) {
            return $this->sendPleaseAddHome($reply_token, 'payment');
        }

        foreach ($homes as $home) {

            $liffUrl = $this->liff->get('paymentTest', ["meter_no" => $home->id]);
            array_push($cards, ['title' => $home->name, 'description' => 'ดูรายละเอียดค่าไฟฟ้าของคุณ', 'url' => $liffUrl]);
        }

        $generateCard = $this->generateCard(
            url('images/payment.png'),
            'คลิกตรวจสอบยอดค้างชำระ',
            $cards
        );

        $m = [
            [
                "type" => "flex",
                "altText" => "เช็คยอดค้าง และ ชำระค่าไฟฟ้าได้ที่นี่",
                "contents" => array(
                    'type' => 'carousel',
                    'contents' => $generateCard,

                ),
            ],
        ];

        $data = [
            'replyToken' => $reply_token,
            'messages' => $m,
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        // Log::create([
        //     "message" => 'add json payments',
        //     "post_body" => $post_body
        // ]);

        $send_result = $this->send_reply_message($post_body);

        // Log::create([
        //     'message' => 'Result: ' . $send_result . '\r\n',
        //     'reply_token' => $reply_token,
        //     'post_body' => $post_body,
        // ]);

        return $send_result;
    }

    public function showEdocList($reply_token, $user_id)
    {

        $cards = [];

        $user = User::where('line_user_id', $user_id)->first();

        $homes = Home::where('user_id', $user->id)->get();

        if (!count($homes)) {
            return $this->sendPleaseAddHome($reply_token, 'edoc');
        }

        foreach ($homes as $home) {
            $liffUrl = $this->liff->get('eBill', ["meter_no" => $home->id]);
            array_push($cards, ['title' => $home->name, 'description' => 'ส่งคำร้องขอรับเอกสารออนไลน์', 'url' => $liffUrl]);
        }

        $generateCard = $this->generateCard(
            url('images/document.png'),
            'แจ้งรับเอกสาร',
            $cards
        );

        array_push($generateCard, $this->verifyCard());


        $m = [
            [
                "type" => "flex",
                "altText" => "แจ้งการขอรับเอกสาร ใบแจ้งค่าไฟฟ้า ใบแจ้งเตือน และ ใบแจ้งตัด ",
                "contents" => array(
                    'type' => 'carousel',
                    'contents' => $generateCard,

                ),
            ],
        ];

        $data = [
            'replyToken' => $reply_token,
            'messages' => $m,
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        // Log::create([
        //     "message" => 'add json payments',
        //     "post_body" => $post_body
        // ]);

        $send_result = $this->send_reply_message($post_body);

        // Log::create([
        //     'message' => 'Result: ' . $send_result . '\r\n',
        //     'reply_token' => $reply_token,
        //     'post_body' => $post_body,
        // ]);
        return $send_result;
    }

    public function showEdocListTest($reply_token, $user_id)
    {

        $cards = [];

        $user = User::where('line_user_id', $user_id)->first();

        $homes = Home::where('user_id', $user->id)->get();

        if (!count($homes)) {
            return $this->sendPleaseAddHome($reply_token, 'edoc');
        }

        foreach ($homes as $home) {
            $liffUrl = $this->liff->get('eBillTest', ["meter_no" => $home->id]);
            array_push($cards, ['title' => $home->name . '(ทดสอบ)', 'description' => 'ส่งคำร้องขอรับเอกสารออนไลน์', 'url' => $liffUrl]);
        }

        $generateCard = $this->generateCard(
            url('images/document.png'),
            'แจ้งรับเอกสาร',
            $cards
        );

        $m = [
            [
                "type" => "flex",
                "altText" => "แจ้งการขอรับเอกสาร ใบแจ้งค่าไฟฟ้า ใบแจ้งเตือน และ ใบแจ้งตัด ",
                "contents" => array(
                    'type' => 'carousel',
                    'contents' => $generateCard,

                ),
            ],
        ];

        $data = [
            'replyToken' => $reply_token,
            'messages' => $m,
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        // Log::create([
        //     "message" => 'add json payments',
        //     "post_body" => $post_body
        // ]);

        $send_result = $this->send_reply_message($post_body);

        // Log::create([
        //     'message' => 'Result: ' . $send_result . '\r\n',
        //     'reply_token' => $reply_token,
        //     'post_body' => $post_body,
        // ]);
        return $send_result;
    }


    public function showSettingList($reply_token, $user_id)
    {

        $m = [
            array(
                'type' => 'text',
                'text' => 'กรุณาเลือก จัดการข้อมูลโปรไฟล์ | จัดการข้อมูลบ้าน ',
                'quickReply' => array(
                    'items' => array(
                        0 => array(
                            'type' => 'action',
                            'action' =>
                            [
                                'type' => 'message',
                                'label' => 'จัดการข้อมูลโปรไฟล์',
                                'text' => 'จัดการข้อมูลโปรไฟล์',
                            ],
                        ),
                        1 => array(
                            'type' => 'action',
                            'action' => array(
                                'type' => 'message',
                                'label' => 'จัดการข้อมูลบ้าน',
                                'text' => 'จัดการข้อมูลบ้าน',
                            ),
                        ),
                        // 2 =>
                        // array(
                        //     'type' => 'action',
                        //     'action' =>
                        //     array(
                        //         'type' => 'message',
                        //         'label' => 'อื่นๆ',
                        //         'text' => 'อื่นๆ',
                        //     ),
                        // ),
                        // 3 =>
                        // array(
                        //     'type' => 'action',
                        //     'action' =>
                        //     array(
                        //         'type' => 'message',
                        //         'label' => 'ขอ token',
                        //         'text' => $user_id,
                        //     ),
                        // ),
                    ),
                ),
            ),
        ];

        $data = [
            'replyToken' => $reply_token,
            'messages' => $m,
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        $send_result = $this->send_reply_message($post_body);

        // Log::create([
        //     'message' => 'Result: ' . $send_result . '\r\n',
        //     'reply_token' => $reply_token,
        //     'post_body' => $post_body,
        // ]);

        return $send_result;
    }

    public function showSettingProfileList($reply_token, $user_id)
    {

        $m = [
            [
                "type" => 'flex',
                "altText" => 'แก้ไขข้อมูลส่วนตัว คลิก',
                "contents" => array(
                    'type' => 'bubble',
                    'footer' => array(
                        'type' => 'box',
                        'layout' => 'vertical',
                        'contents' => array(
                            0 => array(
                                'type' => 'button',
                                'style' => 'primary',
                                'color' => '#f37022',
                                'action' => array(
                                    'type' => 'uri',
                                    'label' => 'จัดการโปรไฟล์',
                                    'uri' => $this->liff->get('profile'),
                                ),
                            ),
                        ),
                    ),
                ),
            ],
        ];

        $data = [
            'replyToken' => $reply_token,
            'messages' => $m,
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        // return $post_body;

        $send_result = $this->send_reply_message($post_body);

        // Log::create([
        //     'message' => 'Result: ' . $send_result . '\r\n',
        //     'reply_token' => $reply_token,
        //     'post_body' => $post_body,
        // ]);
        return $send_result;
    }

    public function showStaffList($user_id)
    {

        $curl = curl_init();
        $user = User::where('line_user_id', $user_id)->first();

        $loadConfigByKey = function ($config) use ($user) {
            $conf = Config::where('key', $config)->first();
            $t = ($conf ? $conf->value : 'https://mea.or.th');
            return $t . '?line_user_id=' . $user->line_user_id . '&staff_id=' . $user->staff_id;
        };

        $data = array(
            'to' => $user_id,
            'messages' => array(
                0 => array(
                    'type' => 'flex',
                    'altText' => 'กรุณาเลือกการ์ด 1 รายการเพื่อใช้งานฟังก์ชันดังกล่าว',
                    'contents' => array(
                        'type' => 'carousel',
                        'contents' => array(
                            0 => array(
                                'type' => 'bubble',
                                'hero' => array(
                                    'type' => 'image',
                                    'url' => url('assets/images/salary.png'),
                                    'size' => 'full',
                                    'aspectRatio' => '20:13',
                                    'aspectMode' => 'cover',
                                    'action' => array(
                                        'type' => 'uri',
                                        'uri' => $loadConfigByKey('staff_salary_link'),
                                    ),
                                ),
                                'body' => array(
                                    'type' => 'box',
                                    'layout' => 'vertical',
                                    'spacing' => 'md',
                                    'action' => array(
                                        'type' => 'uri',
                                        'uri' => $loadConfigByKey('staff_salary_link'),
                                    ),
                                    'contents' => array(
                                        0 => array(
                                            'type' => 'text',
                                            'text' => 'ตรวจสอบเงินเดือนแผนกบุคคล',
                                            'size' => 'lg',
                                            'weight' => 'bold',
                                            'align' => 'start',
                                        ),
                                        1 => array(
                                            'type' => 'text',
                                            'text' => 'สะดวกสามารถตรวจสอบเงินเดือนได้ง่ายๆ',
                                            'wrap' => true,
                                            'color' => '#aaaaaa',
                                            'size' => 'sm',
                                            'align' => 'start',
                                        ),
                                    ),
                                ),
                                'footer' => array(
                                    'type' => 'box',
                                    'layout' => 'vertical',
                                    'contents' => array(
                                        0 => array(
                                            'type' => 'spacer',
                                            'size' => 'sm',
                                        ),
                                        1 => array(
                                            'type' => 'separator',
                                        ),
                                        2 => array(
                                            'type' => 'button',
                                            'action' => array(
                                                'type' => 'uri',
                                                'label' => 'เลือกที่นี่',
                                                'uri' => $loadConfigByKey('staff_salary_link'),
                                            ),
                                            'color' => '#f37022',
                                        ),
                                    ),
                                ),
                            ),
                            1 => array(
                                'type' => 'bubble',
                                'hero' => array(
                                    'type' => 'image',
                                    'url' => url('assets/images/news_update.png'),
                                    'size' => 'full',
                                    'aspectRatio' => '20:13',
                                    'aspectMode' => 'cover',
                                    'action' => array(
                                        'type' => 'uri',
                                        'uri' => $loadConfigByKey('staff_news_link'),
                                    ),
                                ),
                                'body' => array(
                                    'type' => 'box',
                                    'layout' => 'vertical',
                                    'spacing' => 'md',
                                    'action' => array(
                                        'type' => 'uri',
                                        'uri' => $loadConfigByKey('staff_news_link'),
                                    ),
                                    'contents' => array(
                                        0 => array(
                                            'type' => 'text',
                                            'text' => 'ข่าวสารอัพเดท ประกาศจาก MEA',
                                            'size' => 'lg',
                                            'weight' => 'bold',
                                            'align' => 'start',
                                        ),
                                        1 => array(
                                            'type' => 'text',
                                            'text' => 'ติดตามข่าว ครบทุกเรื่องการไฟฟ้า',
                                            'wrap' => true,
                                            'color' => '#aaaaaa',
                                            'size' => 'sm',
                                            'align' => 'start',
                                        ),
                                    ),
                                ),
                                'footer' => array(
                                    'type' => 'box',
                                    'layout' => 'vertical',
                                    'contents' => array(
                                        0 => array(
                                            'type' => 'spacer',
                                            'size' => 'sm',
                                        ),
                                        1 => array(
                                            'type' => 'separator',
                                        ),
                                        2 => array(
                                            'type' => 'button',
                                            'action' => array(
                                                'type' => 'uri',
                                                'label' => 'เลือกที่นี่',
                                                'uri' => $loadConfigByKey('staff_news_link'),
                                            ),
                                            'color' => '#f37022',
                                        ),
                                    ),
                                ),
                            ),
                            2 => array(
                                'type' => 'bubble',
                                'hero' => array(
                                    'type' => 'image',
                                    'url' => url('assets/images/hr_doc.png'),
                                    'size' => 'full',
                                    'aspectRatio' => '20:13',
                                    'aspectMode' => 'cover',
                                    'action' => array(
                                        'type' => 'uri',
                                        'uri' => $loadConfigByKey('staff_hr_doc_link'),
                                    ),
                                ),
                                'body' => array(
                                    'type' => 'box',
                                    'layout' => 'vertical',
                                    'spacing' => 'md',
                                    'action' => array(
                                        'type' => 'uri',
                                        'uri' => $loadConfigByKey('staff_hr_doc_link'),
                                    ),
                                    'contents' => array(
                                        0 => array(
                                            'type' => 'text',
                                            'text' => 'เอกสารจากทาง HR แผนกบุคคล',
                                            'size' => 'lg',
                                            'weight' => 'bold',
                                            'align' => 'start',
                                        ),
                                        1 => array(
                                            'type' => 'text',
                                            'text' => 'รวดเร็วเรื่องเอกสาร',
                                            'wrap' => true,
                                            'color' => '#aaaaaa',
                                            'size' => 'sm',
                                            'align' => 'start',
                                        ),
                                    ),
                                ),
                                'footer' => array(
                                    'type' => 'box',
                                    'layout' => 'vertical',
                                    'contents' => array(
                                        0 => array(
                                            'type' => 'spacer',
                                            'size' => 'sm',
                                        ),
                                        1 => array(
                                            'type' => 'separator',
                                        ),
                                        2 => array(
                                            'type' => 'button',
                                            'action' => array(
                                                'type' => 'uri',
                                                'label' => 'เลือกที่นี่',
                                                'uri' => $loadConfigByKey('staff_hr_doc_link'),
                                            ),
                                            'color' => '#f37022',
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        );

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        $send_result = $this->send_push_message($post_body);

        return $send_result;
    }

    public function showOutsoruceList($user_id)
    {

        $curl = curl_init();
        $user = User::where('line_user_id', $user_id)->first();

        $loadConfigByKey = function ($config) use ($user) {
            $conf = Config::where('key', $config)->first();
            $t = ($conf ? $conf->value : 'https://mea.or.th');
            return $t . '?line_user_id=' . $user->line_user_id . '&outsource_id=' . $user->outsource_id;
        };

        $data = array(
            'to' => $user_id,
            'messages' => array(
                0 => array(
                    'type' => 'flex',
                    'altText' => 'กรุณาเลือกการ์ด 1 รายการเพื่อใช้งานฟังก์ชันดังกล่าว',
                    'contents' => array(
                        'type' => 'carousel',
                        'contents' => array(
                            0 => array(
                                'type' => 'bubble',
                                'hero' => array(
                                    'type' => 'image',
                                    'url' => url('assets/images/sendwork.png'),
                                    'size' => 'full',
                                    'aspectRatio' => '20:13',
                                    'aspectMode' => 'cover',
                                    'action' => array(
                                        'type' => 'uri',
                                        'uri' => $loadConfigByKey('outsource_sendwork_link'),
                                    ),
                                ),
                                'body' => array(
                                    'type' => 'box',
                                    'layout' => 'vertical',
                                    'spacing' => 'md',
                                    'action' => array(
                                        'type' => 'uri',
                                        'uri' => $loadConfigByKey('outsource_sendwork_link'),
                                    ),
                                    'contents' => array(
                                        0 => array(
                                            'type' => 'text',
                                            'text' => 'ใบแจ้ง Orders กดรับใบสั่งงาน',
                                            'size' => 'lg',
                                            'weight' => 'bold',
                                            'align' => 'start',
                                        ),
                                        1 => array(
                                            'type' => 'text',
                                            'text' => 'ดูใบสั่งงานของคุณ',
                                            'wrap' => true,
                                            'color' => '#aaaaaa',
                                            'size' => 'sm',
                                            'align' => 'start',
                                        ),
                                    ),
                                ),
                                'footer' => array(
                                    'type' => 'box',
                                    'layout' => 'vertical',
                                    'contents' => array(
                                        0 => array(
                                            'type' => 'spacer',
                                            'size' => 'sm',
                                        ),
                                        1 => array(
                                            'type' => 'separator',
                                        ),
                                        2 => array(
                                            'type' => 'button',
                                            'action' => array(
                                                'type' => 'uri',
                                                'label' => 'เลือกที่นี่',
                                                'uri' => $loadConfigByKey('outsource_sendwork_link'),
                                            ),
                                            'color' => '#f37022',
                                        ),
                                    ),
                                ),
                            ),
                            1 => array(
                                'type' => 'bubble',
                                'hero' => array(
                                    'type' => 'image',
                                    'url' => url('assets/images/document.png'),
                                    'size' => 'full',
                                    'aspectRatio' => '20:13',
                                    'aspectMode' => 'cover',
                                    'action' => array(
                                        'type' => 'uri',
                                        'uri' => $loadConfigByKey('outsource_doc_link'),
                                    ),
                                ),
                                'body' => array(
                                    'type' => 'box',
                                    'layout' => 'vertical',
                                    'spacing' => 'md',
                                    'action' => array(
                                        'type' => 'uri',
                                        'uri' => $loadConfigByKey('outsource_doc_link'),
                                    ),
                                    'contents' => array(
                                        0 => array(
                                            'type' => 'text',
                                            'text' => 'เอกสารเพิ่มเติมอื่นๆ',
                                            'size' => 'lg',
                                            'weight' => 'bold',
                                            'align' => 'start',
                                        ),
                                        1 => array(
                                            'type' => 'text',
                                            'text' => 'ตรวจสอบเอกสารอื่นๆได้ทันที',
                                            'wrap' => true,
                                            'color' => '#aaaaaa',
                                            'size' => 'sm',
                                            'align' => 'start',
                                        ),
                                    ),
                                ),
                                'footer' => array(
                                    'type' => 'box',
                                    'layout' => 'vertical',
                                    'contents' => array(
                                        0 => array(
                                            'type' => 'spacer',
                                            'size' => 'sm',
                                        ),
                                        1 => array(
                                            'type' => 'separator',
                                        ),
                                        2 => array(
                                            'type' => 'button',
                                            'action' => array(
                                                'type' => 'uri',
                                                'label' => 'เลือกที่นี่',
                                                'uri' => $loadConfigByKey('outsource_doc_link'),
                                            ),
                                            'color' => '#f37022',
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        );

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        $send_result = $this->send_push_message($post_body);

        return $send_result;
    }

    public function showSettingOtherList($reply_token, $user_id)
    {

        $m = [
            [
                "type" => 'flex',
                "altText" => 'ข้อมูลพนักงาน กฟน. พนักงาน outsource  และ เจ้าของกิจการบ้านจัดสรร',
                "contents" => array(
                    'type' => 'carousel',
                    'contents' => array(
                        0 => array(
                            'type' => 'bubble',
                            'hero' => array(
                                'type' => 'image',
                                'url' => url('assets/images/owner_condo.png'),
                                'size' => 'full',
                                'aspectRatio' => '20:13',
                                'aspectMode' => 'cover',
                                'action' => array(
                                    'type' => 'uri',
                                    'uri' => $this->liff->get('loginOwner'),
                                ),
                            ),
                            'body' => array(
                                'type' => 'box',
                                'layout' => 'vertical',
                                'spacing' => 'md',
                                'action' => array(
                                    'type' => 'uri',
                                    'uri' => $this->liff->get('loginOwner'),
                                ),
                                'contents' => array(
                                    0 => array(
                                        'type' => 'text',
                                        'text' => 'บริการขอใช้ไฟฟ้าเพิ่ม',
                                        'size' => 'lg',
                                        'weight' => 'bold',
                                        'align' => 'start',
                                    ),
                                    1 => array(
                                        'type' => 'text',
                                        'text' => 'ขอใช้ไฟฟ้าใหม่ ง่ายนิดเดียว',
                                        'wrap' => true,
                                        'color' => '#aaaaaa',
                                        'size' => 'sm',
                                        'align' => 'start',
                                    ),
                                ),
                            ),
                            'footer' => array(
                                'type' => 'box',
                                'layout' => 'vertical',
                                'contents' => array(
                                    0 => array(
                                        'type' => 'spacer',
                                        'size' => 'sm',
                                    ),
                                    1 => array(
                                        'type' => 'separator',
                                    ),
                                    2 => array(
                                        'type' => 'button',
                                        'action' => array(
                                            'type' => 'uri',
                                            'label' => 'เลือกที่นี่',
                                            'uri' => $this->liff->get('loginOwner'),
                                        ),
                                        'color' => '#f37022',
                                    ),
                                ),
                            ),
                        ),
                        1 => array(
                            'type' => 'bubble',
                            'hero' => array(
                                'type' => 'image',
                                'url' => url('assets/images/staff_mea.png'),
                                'size' => 'full',
                                'aspectRatio' => '20:13',
                                'aspectMode' => 'cover',
                                'action' => array(
                                    'type' => 'uri',
                                    'uri' => $this->liff->get('loginOwner'),
                                ),
                            ),
                            'body' => array(
                                'type' => 'box',
                                'layout' => 'vertical',
                                'spacing' => 'md',
                                'action' => array(
                                    'type' => 'uri',
                                    'uri' => $this->liff->get('loginOwner'),
                                ),
                                'contents' => array(
                                    0 => array(
                                        'type' => 'text',
                                        'text' => 'เข้าสู่ระบบ',
                                        'size' => 'lg',
                                        'weight' => 'bold',
                                        'align' => 'start',
                                    ),
                                    1 => array(
                                        'type' => 'text',
                                        'text' => 'เข้าสู่ระบบได้ทันที',
                                        'wrap' => true,
                                        'color' => '#aaaaaa',
                                        'size' => 'sm',
                                        'align' => 'start',
                                    ),
                                ),
                            ),
                            'footer' => array(
                                'type' => 'box',
                                'layout' => 'vertical',
                                'contents' => array(
                                    0 => array(
                                        'type' => 'spacer',
                                        'size' => 'sm',
                                    ),
                                    1 => array(
                                        'type' => 'separator',
                                    ),
                                    2 => array(
                                        'type' => 'button',
                                        'action' => array(
                                            'type' => 'uri',
                                            'label' => 'เลือกที่นี่',
                                            'uri' => $this->liff->get('loginOwner'),
                                        ),
                                        'color' => '#f37022',
                                    ),
                                ),
                            ),
                        ),
                        2 => array(
                            'type' => 'bubble',
                            'hero' => array(
                                'type' => 'image',
                                'url' => url('assets/images/outsource.png'),
                                'size' => 'full',
                                'aspectRatio' => '20:13',
                                'aspectMode' => 'cover',
                                'action' => array(
                                    'type' => 'uri',
                                    'uri' => $this->liff->get('outsource'),
                                ),
                            ),
                            'body' => array(
                                'type' => 'box',
                                'layout' => 'vertical',
                                'spacing' => 'md',
                                'action' => array(
                                    'type' => 'uri',
                                    'uri' => $this->liff->get('outsource'),
                                ),
                                'contents' => array(
                                    0 => array(
                                        'type' => 'text',
                                        'text' => 'เข้าสู่ระบบ',
                                        'size' => 'lg',
                                        'weight' => 'bold',
                                        'align' => 'start',
                                    ),
                                    1 => array(
                                        'type' => 'text',
                                        'text' => 'เข้าสู่ระบบได้ทันที',
                                        'wrap' => true,
                                        'color' => '#aaaaaa',
                                        'size' => 'sm',
                                        'align' => 'start',
                                    ),
                                ),
                            ),
                            'footer' => array(
                                'type' => 'box',
                                'layout' => 'vertical',
                                'contents' => array(
                                    0 => array(
                                        'type' => 'spacer',
                                        'size' => 'sm',
                                    ),
                                    1 => array(
                                        'type' => 'separator',
                                    ),
                                    2 => array(
                                        'type' => 'button',
                                        'action' => array(
                                            'type' => 'uri',
                                            'label' => 'เลือกที่นี่',
                                            'uri' => $this->liff->get('outsource'),
                                        ),
                                        'color' => '#f37022',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ],
        ];

        $data = [
            'replyToken' => $reply_token,
            'messages' => $m,
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        $send_result = $this->send_reply_message($post_body);

        // Log::create([
        //     'message' => 'Result: ' . $send_result . '\r\n',
        //     'reply_token' => $reply_token,
        //     'post_body' => $post_body,
        // ]);
    }
    public function showSettingHomeList($reply_token, $user_id)
    {

        $cards = [];

        $user = User::where('line_user_id', $user_id)->first();

        $homes = Home::where('user_id', $user->id)->get();

        if (!count($homes)) {

            return $this->sendPleaseAddHome($reply_token, 'setting');
        }

        foreach ($homes as $home) {
            $liffUrl = $this->liff->get('homeSetting', ["meter_no" => $home->id]);
            array_push($cards, ['title' => $home->name, 'description' => 'แก้ไขข้อมูลบ้าน และยกเลิกการรับเอกสารออนไลน์', 'url' => $liffUrl]);
        }

        $generateCard = $this->generateCard(
            url('images/manage.png'),
            'คลิกจัดการข้อมูลบ้าน',
            $cards
        );

        array_push($generateCard, $this->verifyCard());


        $m = [
            [
                "type" => "flex",
                "altText" => "เพิ่มหรือแก้ไข ข้อมูลบ้าน",
                "contents" => array(
                    'type' => 'carousel',
                    'contents' => $generateCard,
                ),
            ],
        ];

        $data = [
            'replyToken' => $reply_token,
            'messages' => $m,
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        $send_result = $this->send_reply_message($post_body);

        // Log::create([
        //     'message' => 'Result: ' . $send_result . '\r\n',
        //     'reply_token' => $reply_token,
        //     'post_body' => $post_body,
        // ]);

        return $send_result;
    }

    public function showReportList($reply_token, $user_id)
    {

        $cards = [];

        $user = User::where('line_user_id', $user_id)->first();

        $homes = Home::where('user_id', $user->id)->get();

        // if (!count($homes)) {
        //     $this->sendPleaseAddHome($reply_token, 'แจ้งไฟฟ้าขัดข้อง');
        //     return false;
        // }

        array_push($cards, ['title' => 'แจ้งเหตุไฟฟ้า', 'description' => 'หากต้องการแจ้งเหตุไฟฟ้าขัดข้องพื้นที่อื่นๆ', 'url' => $this->liff->get('reportOutage')]);

        if (count($homes)) {
            foreach ($homes as $home) {
                $liffUrl = $this->liff->get('reportHome', ["meter_no" => $home->id]);
                array_push($cards, ['title' => $home->name, 'description' => 'หากต้องการแจ้งเหตุไฟขัดข้องที่บ้านของคุณ', 'url' => $liffUrl]);
            }
        }

        $generateCard = $this->generateCard(
            url('assets/images/report_v2.png'),
            'คลิกเพื่อแจ้งเหตุ',
            $cards
        );

        if (!count($homes)) {
            $card = $this->generatePleaseAddHomeCard('report');
            array_push($generateCard, $card);
            // array_push($generateCard, $this->verifyCard());
        }

        $m = [
            [
                "type" => "flex",
                "altText" => "เลือกบ้านของคุณเพื่อแจ้งไฟฟ้าดับ",
                "contents" => array(
                    'type' => 'carousel',
                    'contents' => $generateCard,

                ),
            ],
        ];

        $data = [
            'replyToken' => $reply_token,
            'messages' => $m,
        ];

        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        // Log::create([
        //     "message" => 'add json payments',
        //     "post_body" => $post_body
        // ]);

        $send_result = $this->send_reply_message($post_body);

        // Log::create([
        //     'message' => 'Result: ' . $send_result . '\r\n',
        //     'reply_token' => $reply_token,
        //     'post_body' => $post_body,
        // ]);

        return $send_result;
    }
    public function message(Request $request)
    {

        // PushMessage Custom Message
        $msg = $request->message;
        $id = $request->resp_id;
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient(env('LINE_MESSAGING_TOKEN'));
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => env('LINE_MESSAGING_SECRET')]);

        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($msg);

        $response = $bot->pushMessage($id, $textMessageBuilder);
        if ($response->isSucceeded()) {
            PushMessage::create([
                "resp_id" => $id,
                "message" => $msg,
            ]);
            return 'Succeeded!';
        }

        // Failed
        return $response->getHTTPStatus() . ' ' . $response->getRawBody();
    }

    public function pushMessage(Request $request)
    {
        // PushMessage By Case
        $msg = "";
        $id = $request->line_uid;
        $type = $request->type;
        $ca = $request->ca;
        $trans_id = $request->trans_id;
        $status = $request->status;

        $validatedData = $this->validate(
            $request,
            [
                'line_uid' => 'required|max:255',
                'type' => 'required|max:255',
                'ca' => 'required|max:255',
                'status' => 'required|max:2555',

            ]
        );

        if ($type == 'LINE-PAYMENT-NOTIFY') {
            if ($status == 'SUCCESS') {
                $msg = "ทำการชำระเงินผ่านช่องทาง Rabbit LINE Pay สำเร็จ | CA: {$ca}, รหัสอ้างอิง: {$trans_id}";
            } else if ($status == 'FAIL') {
                $msg = "ขออภัยการชำระเงินผ่านช่องทาง Rabbit LINE Pay ของคุณทำรายการไม่สำเร็จ | CA: {$ca}";
            } else {
                return response()->json([
                    "message" => "status not found.",
                ], 400);
            }
        } else {
            return response()->json([
                "message" => "type not found.",
            ], 400);
        }

        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient(env('LINE_MESSAGING_TOKEN'));
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => env('LINE_MESSAGING_SECRET')]);

        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($msg);

        $response = $bot->pushMessage($id, $textMessageBuilder);
        if ($response->isSucceeded()) {
            PushMessage::create([
                "resp_id" => $id,
                "message" => $msg,
            ]);
            return ["message" => 'Push Message To User Success', "text" => $msg];
        }

        // Failed
        return response()->json([
            "message" => $response->getHTTPStatus() . ' ' . $response->getRawBody(),
        ], 400);
    }

    public function checkRegister($userId)
    {
        $find = User::select('id')->where('line_user_id', $userId)->first();
        $isRegistered = $find ? true : false;
        return $isRegistered;
    }

    public function send_reply_message($post_body)
    {
        try {
            $url = 'https://api.line.me/v2/bot/message/reply';
            $accssToken = env('LINE_MESSAGING_TOKEN');
            $post_header = array('Content-Type: application/json', 'Authorization: Bearer ' . $accssToken);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            return $result ? $result : 'nodata';
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function send_push_message($post_body)
    {

        $url = 'https://api.line.me/v2/bot/message/push';
        $accssToken = env('LINE_MESSAGING_TOKEN');
        $post_header = array('Content-Type: application/json', 'Authorization: Bearer ' . $accssToken);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function generateCard($image, $footer, $datas, $size = '')
    {

        $cards = [];

        foreach ($datas as $data) {

            $set =
                [
                    "type" => "bubble",
                    "hero" => [
                        "type" => "image",
                        'url' => $image,
                        "size" => "full",
                        "aspectRatio" => "20:13",
                        "aspectMode" => "cover",
                        "action" => [
                            "type" => "uri",
                            'uri' => $data['url'],
                        ],
                    ],
                    "body" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "spacing" => "sm",
                        "action" => [
                            "type" => "uri",
                            'uri' => $data['url'],
                        ],
                        "contents" => [
                            [
                                "type" => "text",
                                'text' => $data['title'],
                                "size" => "xl",
                                "weight" => "bold",
                            ],
                            [
                                "type" => "text",
                                'text' => $data['description'],
                                "wrap" => true,
                                "color" => "#aaaaaa",
                                "size" => "md",
                            ],
                            [
                                "type" => "box",
                                "layout" => "vertical",
                                "contents" => [
                                    [
                                        "type" => "spacer",
                                    ],
                                    [
                                        "type" => "separator",
                                    ],
                                ],
                            ],
                        ],
                    ],
                    "footer" => [
                        "type" => "box",
                        "layout" => "vertical",
                        "contents" => [
                            [
                                "type" => "button",
                                "style" => "primary",
                                "color" => "#f37022",

                                "action" => [
                                    "type" => "uri",
                                    'label' => $footer,
                                    'uri' => $data['url'],
                                ],
                            ],
                        ],
                    ],
                ];
            // $set = array(
            //     'type' => 'bubble',
            //     'hero' => array(
            //         'type' => 'image',
            //         'url' => $image,
            //         'size' => 'full',
            //         'aspectRatio' => '20:13',
            //         'aspectMode' => 'cover',
            //     ),
            //     'body' => array(
            //         'type' => 'box',
            //         'layout' => 'vertical',
            //         'contents' => array(
            //             0 => array(
            //                 'type' => 'text',
            //                 'text' => $data['title'],
            //                 'weight' => 'bold',
            //                 'size' => 'xl',
            //             ),
            //             1 => array(
            //                 'type' => 'text',
            //                 'text' => $data['description'],
            //                 'style' => 'normal',
            //                 'decoration' => 'underline',
            //             ),
            //         ),
            //     ),
            //     'footer' => array(
            //         'type' => 'box',
            //         'layout' => 'vertical',
            //         'spacing' => 'sm',
            //         'contents' => array(
            //             0 =>
            //             array(
            //                 'type' => 'spacer',
            //                 'size' => 'sm',
            //             ),
            //             1 => array(
            //                 'type' => 'button',
            //                 'style' => 'link',
            //                 'height' => 'sm',
            //                 'color' => '#f37022',
            //                 'action' => array(
            //                     'type' => 'uri',
            //                     'label' => $footer,
            //                     'uri' => $data['url'],
            //                 ),
            //             ),
            //             2 => array(
            //                 'type' => 'spacer',
            //                 'size' => 'sm',
            //             ),
            //         ),
            //         'flex' => 0,
            //     ),
            // );

            array_push($cards, $set);
        }

        return $cards;
    }
}
