<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Home;

class LinepayController extends Controller
{

    public function index($ui)
    {
        $home = Home::findByHashid($ui);

        $nonce = (string) Str::uuid();
        $ca =
            $home->ca;
        $lineUID = $home->user->line_user_id;
        $secret = env('LINE_PAY_SECRET');
        $url = "https://liff.line.me/1646961788-WZk2YAzP";
        // $url = "https://liff.line-beta.me/1554185081-way2oGDy";

        $urlPath = "/utilities/mea/oa-pay-info/{$ca}/{$lineUID}";
        $sign = base64_encode(hash_hmac('sha256', $secret . $urlPath . $nonce, $secret));

        return [
            'urlFull' => "{$url}{$urlPath}?nonce={$nonce}&sign={$sign}",
            // 'url' => $url,
            // 'urlPath' => $urlPath,
            // 'ca' => $ca,
            // 'lineUID' => $lineUID,
            // 'nonce' => $nonce,
            // 'sign' => $sign
        ];
    }
}