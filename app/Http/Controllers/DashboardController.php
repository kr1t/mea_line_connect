<?php

namespace App\Http\Controllers;

use App\User;
use App\Stat;
use App\Home;
use App\EbillLog;

use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index()
    {
        $newUserCount = User::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $newUserCountPast = User::whereBetween('created_at', [Carbon::now()->subWeeks(1)->startOfWeek(), Carbon::now()->subWeeks(1)->endOfWeek()])->count();
        $totalClick = Stat::whereMonth('created_at', Carbon::now())->count();
        $totalClickPast = Stat::whereMonth('created_at', Carbon::now()->subMonths(1))->count();
        $houseRegisterCount = Home::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $houseRegisterCountPast = Home::whereBetween('created_at', [Carbon::now()->subWeeks(1)->startOfWeek(), Carbon::now()->subWeeks(1)->endOfWeek()])->count();
        $edocRegisterCount = EbillLog::where('status', 1)->whereMonth('created_at', Carbon::now())->count();
        $edocRegisterCountPast = EbillLog::where('status', 1)->whereMonth('created_at', Carbon::now()->subMonths(1))->count();

        return [
            "result" => [
                "newUserCount" => number_format($newUserCount),
                "newUserCountPast" => number_format($newUserCountPast),
                "totalClick" =>  number_format($totalClick),
                "totalClickPast" => number_format($totalClickPast),
                "houseRegisterCount" => number_format($houseRegisterCount),
                "houseRegisterCountPast" => number_format($houseRegisterCountPast),
                "edocRegisterCount" => number_format($edocRegisterCount),
                "edocRegisterCountPast" => number_format($edocRegisterCountPast)

            ]
        ];
    }
}