<?php

namespace App\Http\Controllers;

use App\Stat;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatController extends Controller
{
    public function index(Request $request)
    {

        $by_user = $request->by_user;
        $date_from = $request->date_from;
        $date_to = $request->date_to;

        if (!$date_to && $date_from) {
            $date_to = Carbon::now()->format('Y-m-d');
        } else if (!$date_from && $date_to) {
            $date_from = Carbon::now()->add(-1000, 'year')->format('Y-m-d');
        }

        $query = $count_page = Stat::groupBy('page_name')->select('page_name', DB::raw('count(*) as total'));

        if ($date_from) {
            $query->whereBetween('created_at', [$date_from, $date_to . " 23:59:59"]);
        }


        $count_page = $query->get();

        if ($by_user == 'by_user') {
            foreach ($count_page as $page) {
                $query = Stat::where('page_name', $page->page_name)->groupBy(['user_id', 'page_name'])->select('page_name', 'user_id');
                if ($date_from) {
                    $query->whereBetween('created_at', [$date_from, $date_to . " 23:59:59"]);
                }
                $page->total = $query->get()->count();
            }
        }

        // $count_page_by_user = Stat::groupBy(['user_id', 'page_name'])->select('page_name', 'user_id', DB::raw('count(*) as total'))->get();

        return [
            "count_page" => $count_page,
            "date_from" => $date_from,
            "date_to" => $date_to
            // "count_page_by_user" =>  $count_page_by_user
        ];
    }
    public function store(Request $request)
    {
        $user = User::where('line_user_id', $request->line_user_id)->first();
        $stat = Stat::create([
            'ip' => $request->ip(),
            'page_name' => $request->page_name,
            'user_id' => $user ? $user->id : null
        ]);
        return 'ok';
    }
}
