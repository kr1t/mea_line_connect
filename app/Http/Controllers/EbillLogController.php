<?php

namespace App\Http\Controllers;

use App\EbillLog;
use Illuminate\Http\Request;

class EbillLogController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->q;
        $ebills = EbillLog::with(['user'])->latest()->paginate(10);

        $count = EbillLog::count();

        return [
            "ebills" => $ebills,
            "count" => $count
        ];
    }
}