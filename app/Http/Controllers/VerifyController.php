<?php

namespace App\Http\Controllers;

use App\Home;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerifyController extends Controller
{
    public function __construct()
    {
        $this->homes = [];
        $this->statusCode = 200;
    }
    public function index(Request $request)
    {
        $this->user = User::where('line_user_id', $request->user_id)->first();


        $this->setPayload($request);

        $this->saveUser = [
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "id_card" => $request->id_card,
            "birthday" => $request->birthday
        ];

        $this->findCaUi();

        if ($this->statusCode != 200) {

            return [
                "status" => $this->statusCode,
                "homes" => [],
                'body' => $this->body,
                'h' => $this->homes,
                'message' => $this->message,
                // 'xapi' => $this->xapi,
            ];
        }

        // return $this->homes;


        $this->user->update($this->saveUser);

        $user = $this->user;

        $homes = collect($this->homes)->map(function ($home) use ($user) {
            $ca = str_pad((int) $home['contractNumber'], 9, '0', STR_PAD_LEFT);

            $findUserHome = Home::where('ca', (string) $ca)->where('user_id', $user->id)->first();
            $home['added'] = $findUserHome ? true : false;
            $home['name'] = $findUserHome ? $findUserHome->name : false;
            $home['active'] = $findUserHome ? true : false;
            $home['user_id'] = $user->id;
            $home['contractNumber'] = '0' . (int)$home['contractNumber'];
            $home['installationNumber'] = (int)$home['installationNumber'];

            return $home;
        });

        return [
            "status" => $this->statusCode,
            "homes" => $homes,
            'body' => $this->body,
            'h' => $this->homes,
            'message' => $this->message,
            // 'xapi' => $this->xapi,
        ];
    }

    public function setPayload($request)
    {
        $body = [
            "cid" => "{$request->id_card}",
            "fisrtname" => $request->first_name,
            "lastname" => $request->last_name,
            "dob" => str_replace("-", "", $request->birthday),
            "laserid" => "{$request->laser_id}",
        ];
        $this->body = json_encode($body);
        return $body;
    }

    public function findCaUi()
    {
        if (env('APP_ENV') == 'local') {
            return $this->mockUpResponse();
        }

        $curl = curl_init();

        $xapi = env('VERIFY_X_API_KEY');
        $this->xapi = $xapi;

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('VERIFY_API_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $this->body,
            CURLOPT_HTTPHEADER => array(
                "x-api-key: {$xapi}",
                'Content-Type: application/json',
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $homes = json_decode($response, true);
        $this->homes = $homes;
        try {
            if ($homes) {
                if (!array_key_exists('statusCode', $homes)) {
                    $this->statusCode = 200;
                    $this->message = 'success';
                } else {
                    $this->statusCode = $homes['statusCode'];
                    $this->message = $homes['message'] ?? 'error';
                }
            } else {
                $this->statusCode = 500;
                $this->user->update($this->saveUser);
                $this->message = "ไม่พบเครื่องวัดไฟฟ้าของท่านในระบบ";
            }
        } catch (Exception $e) {
            $this->statusCode = 500;
            $this->message = 'error';
        }

        return $homes;
    }

    public function mockUpResponse()
    {
        // generate data by calling methods
        $homes = [
            [
                "contractNumber" => "000016463617",
                "installationNumber" => "0096486914",
                "address" => '1333/838 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 701223499,
                "installationNumber" => 212234899,
                "address" => '1333/738 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 1901223499,
                "installationNumber" => 112234899,
                "address" => '1333/838 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 2701223499,
                "installationNumber" => 212234899,
                "address" => '1333/738 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 3901223499,
                "installationNumber" => 112234899,
                "address" => '1333/838 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 4701223499,
                "installationNumber" => 212234899,
                "address" => '1333/738 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 5901223499,
                "installationNumber" => 112234899,
                "address" => '1333/838 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 6701223499,
                "installationNumber" => 212234899,
                "address" => '1333/738 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 7901223499,
                "installationNumber" => 112234899,
                "address" => '1333/838 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 8701223499,
                "installationNumber" => 212234899,
                "address" => '1333/738 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 9901223499,
                "installationNumber" => 112234899,
                "address" => '1333/838 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
            [
                "contractNumber" => 11701223499,
                "installationNumber" => 212234899,
                "address" => '1333/738 เดอะไลน์วงศ์สว่าง แขวงวงศ์สว่าง เขตบางซื่อ 10800 กรุงเทพมหานคร.',
            ],
        ];

        $this->homes = $homes;
        $this->message = 'success';
        $this->statusCode = 200;

        return $homes;
    }

    public function addHome(Request $request)
    {
        try {
            $user = User::where('line_user_id', $request->line_user_id)->first();

            // return $user;
            $homes = $request->homes;

            if (!count($homes)) {
                return ["status" => 400, "message" => 'กรุณาเลือกบ้านอย่างน้อย 1 รายการ'];
            }

            // return $request->all();
            $homes = collect($homes)->map(function ($home) use ($user) {
                $home['user_id'] = $user->id;
                $home['ca'] = (int) $home['contractNumber'];
                $home['ui'] = (int) $home['installationNumber'];
                $home['ca'] = str_pad($home['ca'], 9, '0', STR_PAD_LEFT);
                return $home;
            });

            $homes->map(function ($home) {
                $checkInvalid = Home::where([['ca', $home['ca']], ['ui', $home['ui']], ['user_id', $home['user_id']]])->first();
                if (!$checkInvalid) {
                    Home::create($home);
                    $home['success'] = true;
                } else {
                    $home['success'] = false;
                }

                return $home;
            });

            return ["status" => 200, "message" => 'เพิ่มบ้านสำเร็จ', "homes" => $homes];
        } catch (\Exception $e) {
            return ["status" => 400, "message" => 'มีบางอย่างผิดพลาด', 'error' => $e];
        }
    }
}
