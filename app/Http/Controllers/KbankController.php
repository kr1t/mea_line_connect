<?php

namespace App\Http\Controllers;

use App\Home;
use App\Kplus;
use App\User;
use Carbon\Carbon;
use Helpers\LineBot;
use Illuminate\Http\Request;

class KbankController extends Controller
{
    public function __construct()
    {
        $this->res = [];
        $this->body = [];
        $this->message = "";
        $this->status = null;
        $this->user = env('KPLUS_USER');
        $this->password = env('KPLUS_PASSWORD');
        $this->reasonMaps = [
            "0000" => 'สามารถตรวจสอบการชำระเงินได้ที่แอปพลิเคชัน K PLUS <br>Payment can be check at K PLUS application',
            "1001" => 'เบอร์มือถือไม่ได้ลงทะเบียนไว้กับ K PLUS กรุณาทำรายการใหม่อีกครั้ง Mobile number is not registered with K PLUS please try again.',
        ];
    }

    public function response()
    {
        // $this->body['userId'] = 'xxx';
        // $this->body['password'] = 'xxx';

        return [
            "status" => $this->status,
            "message" => $this->message,
            "body" => $this->body,
            "res" => $this->res,
        ];
    }

    public function push($ca, Request $request)
    {
        $user = User::whereLineUserId($request->line_user_id)->first();
        $home = Home::whereUuid($ca)->first();

        $this->userId = $user->id;
        $amount = $request->amount;
        $tel = $request->tel;

        $billId = time() . $user->id;

        $amount = (int) number_format($amount, 2, '', '');

        $body = [
            "txType" => "INFO",
            "sourceTxNo" => $billId,
            "userId" => $this->user,
            "password" => $this->password,
            "timestamp" => Carbon::now()->format('YmdHis'),
            "profileId" => env('KPLUS_PROFILE_ID'),
            "mobileService" => "PLUS",
            // "mobileNo" => $user->tel,
            // "mobileNo" => '0659614644',
            "mobileNo" => $tel,
            "language" => "th",
            "refNo1" => $home->ca,
            "refNo2" => null,
            "refNo3" => null,
            "info1En" => "01|Electric bill (MEA)",
            "info2En" => "LINE MEA Connect",
            "info3En" => null,
            "info1Th" => "01|ชำระค่าไฟฟ้า",
            "info2Th" => "LINE MEA Connect",
            "info3Th" => null,
            "amount" => $amount,
            "minAmount" => $amount,
            "txExpiry" => Carbon::now()->format('Ymd233059'),
        ];

        $this->body = $body;

        try {
            return $this->api($body)->mapReason()->response();
        } catch (\Exception $e) {
            return 'error';
            return $this->res;
        }
    }

    public function mapReason()
    {

        try {
            $this->message = $this->reasonMaps[$this->res['reasonCode']] ?? "กรุณาทำรายการใหม่อีกครั้ง Please try again. รหัสข้อผิดพลาด ({$this->res['reasonCode']})";
            $this->status = $this->res['reasonCode'];
        } catch (\Exception $e) {
        }

        return $this;
    }

    public function callback(Request $request)
    {

        $res = [
            "responseID" => request()->requestID,
            "msgCode" => "0000",
            "msgDesc" => "Success",
            "status" => request()->status,
        ];

        $tel = '0' . substr($request->refNo3, 2);

        $kplus = Kplus::where('tel', $tel)->where('ca', $request->refNo1)
            ->whereStatus(1)
            ->latest()->first();

        if (!$kplus) {
            $res['status'] = 'f';
            $res['msgDesc'] = 'failed';

            return response()->json($res);
        }

        $kplus->update([
            "callback_req" => $request->all(),
        ]);

        $user = User::whereId($kplus->user_id)->first();

        $lineUid = $user->line_user_id;

        $status = $request->status;
        $this->bot = new LineBot(env('LINE_MESSAGING_TOKEN'));
        $req = json_encode($request->all());

        $testUids = [
            // 'U60ed970a4bedd9ddaa0f7e1120ff24c8',
            // 'U82740f54e80207cfa39de21970632775',
        ];

        if ($status == 'S') {
            $kplus->update([
                "status" => 2,
            ]);

            $ca = $request->refNo1;
            $kplusAmount = $request->amount;
            $date = Carbon::now()->format('d/m/Y');
            $dateThai = Carbon::now()->addYears(543)->format('d/m/Y');

            $decimal = substr($kplusAmount, -2);
            $number = number_format(substr($kplusAmount, 0, strlen($kplusAmount) - 2));

            $amount = "{$number}.{$decimal}";
            $ca = 'CA' . $ca;
            $this->bot->addText("ขอบคุณที่ชำระค่าไฟ {$ca} ยอด {$amount} เมื่อ {$dateThai} | Thank you for your electricity payment with reference no. {$ca} {$amount} THB on {$date}");
            $this->bot->setUser($lineUid);
            $this->bot->push();

            foreach ($testUids as $testUid) {
                if ($lineUid != $testUid) {
                    $this->bot->setUser($testUid);
                }
            }

            // if (count($testUids)) {
            //     $this->bot->addText("{$req}");
            //     $this->bot->push();
            // }

        }

        if ($status == 'F') {
            $res['status'] = 'f';
            $kplus->update([
                "status" => 0,
            ]);

            // $this->bot->addText("ไม่สำเร็จ");
            // $this->bot->addText("{$req}");
            // $this->bot->setUser($lineUid);

            // foreach ($testUids as $testUid) {
            //     if ($lineUid != $testUid) {
            //         $this->bot->setUser($testUid);
            //     }
            // }
            // $this->bot->push();
        }

        return response()->json($res);
    }

    public function mock($kplus)
    {
        $res = [
            "mockup" => true,
            "reasonCode" => "0000",
            "reasonDesc" => "SUCCESS",
            "txType" => "INFO",
            "kbankTxNo" => "CA41241-4133411-234324",
        ];

        $kplus->update([
            "res" => $res,
        ]);

        $this->res = $res;

        return $this;
    }

    public function api($body)
    {
        $kplus = Kplus::create([
            "bill_id" => $body['sourceTxNo'],
            "user_id" => $this->userId,
            "req" => $body,
            "tel" => $body['mobileNo'],
            "ca" => $body['refNo1'],
        ]);

        // return $this->mock($kplus);

        // $user = $this->user;
        // $password = $this->password;

        // $auth = base64_encode("{$user}:{$password}");

        $curl = curl_init();

        $req = json_encode($body);

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('KPLUS_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => "{$req}",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $res = json_decode($response, true);

        $kplus->update([
            "res" => $res,
        ]);

        $this->res = $res;
        return $this;
    }
}
