<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Home;
use Illuminate\Support\Facades\Validator;

class FFMController extends Controller
{
    private $token = '';

    private function login()
    {

        $curl = curl_init();
        $userName = env('FFM_USERNAME');
        $password = env('FFM_PASSWORD');

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('FFM_API_URL') . "/api/appauthen/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=password&username={$userName}&password={$password}&client_id=line",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $this->token = json_decode($response, true)['access_token'];
    }
    public function reportOutage(Request $request)
    {

        $curl = curl_init();


        $user_id = (env('APP_ENV') == 'local') ?  'U82740f54e80207cfa39de21970632775d' : $request->line_user_id;

        $user = User::where('line_user_id', $user_id)->first();

        if (!$user) {
            return response()->json(['message' => 'user notfound'], 400);
        }

        $name = "{$user->first_name} {$user->last_name}";

        $req = $request->only(['TELNO', 'LAT', 'LONG', 'DESC', 'IMG']);
        $req['NOTIFIER_NAME'] = $name;

        if (isset($_FILES['IMG'])) {
            $req['IMG'] = new \CURLFile($_FILES['IMG']['tmp_name'], $_FILES['IMG']['type'], $_FILES['IMG']['name']);
        }

        $req['NOTIFIER_ADDR'] = '-';
        $req['RESP_URL'] = env('APP_URL') . "api/message";
        $req['RESP_ID'] = $user->line_user_id;

        $this->login();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('FFM_API_URL') . "/api/sendoutage/location",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $req,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer {$this->token}"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        if (($response) == "It's not in mea district area") {
            return response()->json(["message" => "It's not in mea district area"], 400);
        }
        return $response;
    }
    public function reportHome(Request $request)
    {
        $this->login();

        $curl = curl_init();

        $user = User::where('line_user_id', $request->line_user_id)->first();

        if (!$user) {
            return response()->json([
                'message' => 'user notfound'
            ], 400);
        }

        $home = Home::findByHashid($request->meter_no);
        if (!$home) {
            return response()->json([
                'message' => 'home notfound'
            ], 400);
        }
        $name = "{$user->first_name} {$user->last_name}";

        $req = $request->only(['TELNO', 'DESC', 'IMG']);
        $req['NOTIFIER_NAME'] = $name;
        if (isset($_FILES['IMG'])) {
            $req['IMG'] = new \CURLFile($_FILES['IMG']['tmp_name'], $_FILES['IMG']['type'], $_FILES['IMG']['name']);
        }
        $req['RESP_URL'] = env('APP_URL') . "api/message";
        $req['RESP_ID'] = $user->line_user_id;
        $req['METER_NO'] = $home->ui;

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  env('FFM_API_URL') . "/api/sendoutage/meter",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $req,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer {$this->token}"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }
}
