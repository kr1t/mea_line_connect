<?php

namespace App\Http\Controllers;

use App\User;

use App\Exports\UsersExport;
use App\Exports\UsersWithHomeExport;
use App\Exports\HomesExport;

use App\Home;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;
use Carbon\Carbon;

class ExportController extends Controller
{
    public function index()
    {
        return "Hello";
    }

    public function users(Request $request)
    {

        $to = $request->to . ' 23:59:59';
        $from = $request->from;
        $users = User::whereNotNull('line_user_id')->where(function ($q) use ($to, $from) {
            if ($to && $from) {
                $q->whereBetween('created_at', [$from, $to]);
            } else {
                $q->whereBetween('created_at', [Carbon::now()->format('Y-m-d 00:00'), Carbon::now()->format('Y-m-d 23:59')]);
            }
        })->get();
        return Excel::download(new UsersExport($users), 'users-' . date('YmdHis') . '.xlsx');
    }

    public function usersWithHome(Request $request)
    {
        $to = $request->to . ' 23:59:59';
        $from = $request->from;
        $users = User::whereNotNull('line_user_id')->where(function ($q) use ($to, $from) {
            if ($to && $from) {
                $q->whereBetween('created_at', [$from, $to]);
            } else {
                $q->whereBetween('created_at', [Carbon::now()->format('Y-m-d 00:00'), Carbon::now()->format('Y-m-d 23:59')]);
            }
        })->with(['homes'])->get();
        return Excel::download(new UsersWithHomeExport($users), 'users-' . date('YmdHis') . '.xlsx');
    }

    public function homes(Request $request)
    {
        $to = $request->to . ' 23:59:59';
        $from = $request->from;
        $homes = Home::where(function ($q) use ($to, $from) {
            if ($to && $from) {
                $q->whereBetween('created_at', [$from, $to]);
            } else {
                $q->whereBetween('created_at', [Carbon::now()->format('Y-m-d 00:00'), Carbon::now()->format('Y-m-d 23:59')]);
            }
        })->with(['user'])->get();
        return Excel::download(new HomesExport($homes), 'homes-' . date('YmdHis') . '.xlsx');
    }
}
