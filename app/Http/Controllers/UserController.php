<?php

namespace App\Http\Controllers;

use App\Otp;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->q;
        $isAdmin = $request->isAdmin;
        $users = User::where(function ($q) use ($keyword) {
            $q->orWhere('first_name', 'LIKE', "%$keyword%")
                ->orWhere('last_name', 'LIKE', "%$keyword%")
                ->orWhere('tel', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('line_user_id', 'LIKE', "%$keyword%");
        })->where(function ($q) use ($isAdmin) {
            if ($isAdmin) {
                $q->where('role', 'admin');
            }
        })->latest()->paginate(10);
        $count = User::where(function ($q) use ($isAdmin) {
            if ($isAdmin) {
                $q->where('role', 'admin');
            }
        })->count();

        return [
            "users" => $users,
            "count" => $count,
        ];
    }

    public function store(Request $request)
    {

        $user = User::where('email', $request->email)->first();
        if ($user) {
            $user->update([
                'role' => 'admin',
            ]);
        } else {
            $req = $request->all();
            $req['role'] = 'admin';
            $req['password'] = bcrypt($req['password']);
            $user = User::create($req);
        }

        return 'ok';
    }

    public function generateRandomString($length = 10)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function sendOTP($otp, $ref, $tel)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('OTP_API_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('USER' => env('OTP_USER'), 'PASS' => env('OTP_PASSWORD'), 'TO' => $tel, 'CONTENT' => "รหัส OTP {$otp} (Ref: {$ref})"),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function str_replace_first($a, $b, $s)
    {
        $w = strpos($s, $a);
        if ($w === false) {
            return $s;
        }

        return substr($s, 0, $w) . $b . substr($s, $w + strlen($a));
    }

    public function otp(Request $request)
    {
        $isLocal = env('APP_ENV') == 'local';
        $user = User::where('line_user_id', $request->line_user_id)->first();
        $tel = $this->str_replace_first('0', '66', $user->tel);
        $nOtp = rand(123456, 999888);
        if ($isLocal) {
            $nOtp = 123456;
        }
        $randRef = $this->generateRandomString(4);
        $otp = Otp::create([
            'otp' => bcrypt($nOtp),
            'ref' => $randRef,
            'expiry_date' => Carbon::now()->addMinutes(3),
            'user_id' => $user->id,
            'tel' => $tel,
        ]);
        if ($otp) {
            if (!$isLocal) {
                $status = $this->sendOTP($nOtp, $randRef, $tel);
            }
        }

        return ['ref' => $randRef, 'tel' => $user->tel];
    }

    public function delete(Request $request)
    {
        $user = User::where('line_user_id', $request->line_user_id)->first();
        $user->delete();
        return 'ok';
    }
    public function confirmOtp(Request $request)
    {
        $user = User::where('line_user_id', $request->line_user_id)->first();

        $otp = Otp::where([
            [
                'user_id', $user->id,
            ],
            [
                'ref', $request->ref,
            ],
        ])->latest()->first();

        if ($otp->expiry_date < Carbon::now()) {
            return [
                'type' => 'fail',
                'message' => 'OTP หมดอายุ',
            ];
        }

        $check = Hash::check($request->otp, $otp->otp);
        if ($check) {
            $user->update([
                'verify' => 1,
            ]);

            return [
                'type' => 'success',
                'message' => 'ยืนยันบัญชีสำเร็จ',
            ];
        } else {

            return [
                'type' => 'fail',
                'message' => 'OTP ของคุณไม่ถูกต้อง',
            ];
        }

        return [
            'type' => 'fail',
            'message' => 'มีบางอย่างผิดพลาด',
        ];
    }

    public function missionStickerOpen($uid)
    {
        try {
            $curl = curl_init();

            $postBody = [
                "to" => $uid,
                "productType" => "STICKER",
                "productId" => "24779",
                "sendPresentMessage" => true

            ];

            $token = env('LINE_MESSAGING_TOKEN');
            $postBodyJson = json_encode($postBody);
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.line.me/shop/v3/mission',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $postBodyJson,
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer {$token}",
                    'Content-Type: application/json'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $user = User::where('line_user_id', $uid)->first();
            if (!$user->grant_sticker_at) {
                $user->update([
                    "grant_sticker_at" => Carbon::now()
                ]);
            }

            return json_decode($response, true);
        } catch (\Exception $e) {
            return ['message' => 'something error'];
        }
    }

    public function grantSticker(Request $request)
    {
        return $this->missionStickerOpen($request->line_user_id);
    }
    public function del(Request $request)
    {

        $user = User::where('id', $request->id)->first();
        $user->role = 'old_admin';
        $user->save();
        return 'remove admin success';
    }
    public function checkRegistered(Request $request)
    {
        $find = User::where('line_user_id', $request->line_user_id)->first();

        $isRegistered = $find ? true : false;

        return response()->json([
            'message' => '',
            'result' => [
                'isRegistered' => $isRegistered,
                'verify' => $find ? $find->verify : false,
            ],
        ]);
    }

    public function user($userID, Request $request)
    {
        $find = User::where('line_user_id', $userID)->first();

        return response()->json([
            'message' => 'load user success',
            'result' => [
                'user' => $find ? $find : null,
            ],
        ]);
    }

    public function register(Request $request)
    {

        $messages = [
            'first_name.required' => 'กรุณากรอก ชื่อจริง',
            'last_name.required' => 'กรุณากรอก นามสกุล',
            'email.required' => 'กรุณากรอก อีเมล',
            'tel.required' => 'กรุณากรอก เบอร์โทรศัพท์',
            'email.unique' => 'อีเมลนี้ถูกใช้งานแล้ว',
            'tel.unique' => 'เบอร์โทรศัพท์นี้ถูกใช้งานแล้ว',
        ];
        $validatedData = $this->validate(
            $request,
            [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'tel' => 'required|max:12|unique:users',
            ],
            $messages
        );

        $req = $request->all();
        $req['password'] = bcrypt(time());

        $user = User::create($req);

        if (!$user) {
            return 'failed';
        }

        return response()->json([
            'message' => 'Register Success',
        ]);
    }

    public function update(Request $request)
    {

        $user = User::where('line_user_id', $request->line_user_id)->first();

        if ($request->email && $request->tel) {
            $messages = [
                'email.unique' => 'อีเมลนี้ถูกใช้งานแล้ว',
                'tel.unique' => 'เบอร์โทรศัพท์นี้ถูกใช้งานแล้ว',
            ];
            $validatedData = $this->validate(
                $request,
                [
                    'email' => 'required|email|max:255|unique:users,email,' . $user->id,
                    'tel' => 'required|max:12|unique:users,tel,' . $user->id,
                ],
                $messages
            );
        }

        $req = $request->all();
        $req['password'] = bcrypt(time());

        $update = $user->update($req);

        if (!$update) {
            return 'failed';
        }

        return response()->json([
            'message' => 'Register Success',
        ]);
    }
}
