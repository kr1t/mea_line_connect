<?php

namespace App\Http\Controllers;

use App\Home;

class SpaController extends Controller
{
    /**
     * Get the SPA view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uri = request()->path();
        if (request()->meter_no) {
            $home = Home::where('uuid', request()->meter_no)->get();
            if (!count($home)) {
                return view('404');
            }
        }
        return view('spa');
    }

    public function test()
    {
        $uri = request()->path();
        if (request()->meter_no) {
            $home = Home::where('uuid', request()->meter_no)->get();
            if (!count($home)) {
                return view('404');
            }
        }
        return view('spasit');
    }
}
