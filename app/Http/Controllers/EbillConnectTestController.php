<?php

namespace App\Http\Controllers;

use App\EbillLog;
use App\Home;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class EbillConnectTestController extends Controller
{

    public function checkRegister($ui)
    {

        $home = Home::findByHashid($ui);

        $curl = curl_init();

        $email = $home->ebill_email ? $home->ebill_email : $home->user->email;
        $mobileNo = $home->user->tel;

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('EBILLS_API_URL') . "/line_services/checkEbillRegisv5dev",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "ca={$home->ca}&ui={$home->ui}&lineid={$home->user->line_user_id}&email={$email}&mobile_no={$mobileNo}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode(
            $response,
            true
        );

        return Arr::only($response[0], ['STATUS', 'MESSAGE', 'CHANNEL']);
    }

    public function insert($form)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('EBILLS_API_URL') . "/line_services/insert_datav2",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => Arr::query($form),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode(
            $response,
            true
        );

        return $response;
    }

    public function cancel($form)
    {

        $curl
            = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('EBILLS_API_URL') . "/line_services/unsubscribev2",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => Arr::query($form),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode(
            $response,
            true
        );

        return $response;
    }

    public function check($ui, Request $request)
    {

        $home = Home::findByHashid($ui);

        $check = $this->checkRegister($ui);

        if ($check['STATUS'] == 'S' && $check['CHANNEL'] != 'LINE') {
            $insert = $this->insert([
                "ca" => $home->ca,
                "ui" => $home->ui,
                "mobile" => $home->user->tel,
                "email" => $home->user->email,
                "registername" => "{$home->user->first_name} {$home->user->last_name}",
                "noti" => true,
                "lineid" => $home->user->line_user_id,
            ]);
        }
        return $check;
    }

    public function store($ui, Request $request)
    {

        $home = Home::findByHashid($ui);
        $insert = $this->insert([
            "ca" => $home->ca,
            "ui" => $home->ui,
            "mobile" => $home->user->tel,
            "email" => $request->email,
            "registername" => "{$home->user->first_name} {$home->user->last_name}",
            "noti" => true,
            "invoice" => true,
            "lineid" => $home->user->line_user_id,
        ]);

        $home->update([
            "ebill_email" => $request->email,
        ]);

        EbillLog::create([
            "status" => 1,
            "ca" => $home->ca,
            "ui" => $home->ui,
            "user_id" => $home->user_id,
        ]);
        return $insert;
    }

    public function cancelEdoc($ui, Request $request)
    {
        $home = Home::findByHashid($ui);
        $cancel = $this->cancel(
            [
                "ca" => $home->ca,
                "ui" => $home->ui,
                "email" => $home->user->email,
                "lineid" => $home->user->line_user_id,
                "cancle_reason" => $request->cancle_reason,
            ]
        );
        EbillLog::create([
            "status" => 0,
            "ca" => $home->ca,
            "ui" => $home->ui,
            "user_id" => $home->user_id,
        ]);
        return $cancel;
    }
}
