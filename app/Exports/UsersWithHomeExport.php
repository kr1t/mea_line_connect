<?php

namespace App\Exports;

use App\User;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

class UsersWithHomeExport

extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements WithCustomValueBinder, FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $users;

    public function __construct($users = null)
    {
        $this->users = $users ? $users : User::with(['homes'])->get();;
    }


    public function view(): View
    {
        return view('exports.users-with-home', [
            'users' => $this->users
        ]);
    }
}
