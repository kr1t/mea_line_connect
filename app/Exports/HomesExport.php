<?php

namespace App\Exports;

use App\Home;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

class HomesExport

extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements WithCustomValueBinder, FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $homes;

    public function __construct($homes = null)
    {
        $this->homes = $homes ? $homes : Home::with(['user'])->get();
    }

    public function view(): View
    {
        return view('exports.homes', [
            'homes' => $this->homes
        ]);
    }
}
