<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kplus extends Model
{

    protected $fillable = ['bill_id', 'user_id', 'req', 'res', 'status', 'tel', 'ca', 'callback_req'];

    protected $casts = [
        'req' => 'array',
        'res' => 'array',
    ];

    public function getReq()
    {
        return json_encode($this->data);
    }

    public function getRes()
    {
        return json_encode($this->data);
    }
}
