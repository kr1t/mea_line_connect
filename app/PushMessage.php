<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PushMessage extends Model
{
    protected $fillable = ['resp_id', 'message'];
}
