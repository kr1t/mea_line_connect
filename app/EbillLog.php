<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EbillLog extends Model
{
    protected $appends = ['action', 'created_at_text'];
    protected $fillable = ['status', 'ca', 'ui', 'user_id'];

    public function getActionAttribute()
    {
        return $this->status == 0 ? 'ยกเลิกการสมัคร' : 'ทำการสมัคร';
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCreatedAtTextAttribute()
    {
        return $this->created_at ? Carbon::parse($this->created_at)->format('d/m/Y H:i:s') : '-';
    }


    public function home()
    {
        return $this->belongsTo('App\Home', 'home_id', 'id');
    }
}