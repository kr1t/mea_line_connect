<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $fillable = ['otp', 'ref', 'expiry_date', 'user_id', 'tel'];
}
