<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', 'Auth\UserController@current');

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');

    Route::get(
        'dashboard',
        'DashboardController@index'
    );

    Route::get('/users', 'UserController@index');
    Route::post('/users', 'UserController@store');
    Route::post('/users/del', 'UserController@del');

    Route::get('/ebill_logs', 'EbillLogController@index');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});
Route::get('message', 'WebHook@message');
Route::post('message', 'WebHook@message');
Route::post('push/message', 'WebHook@pushMessage');

Route::get('line/user/check/register', 'UserController@checkRegistered');
Route::get('line/user/sticker/grant', 'UserController@grantSticker');
Route::get('line/user/delete', 'UserController@delete');

Route::post('register', 'UserController@register');
Route::post('/user/update', 'UserController@update');
Route::get('push_message', 'PushMessageController@index');
Route::get('log', 'LogController@index');
Route::post('webHook', 'WebHook@index');
// Route::get('webHook', 'WebHook@index');
Route::get('home_validate', 'HomeController@checkValid');
Route::get('home/{ui}', 'HomeController@LoadHome');

Route::post('verify', 'VerifyController@index');
Route::post('verify/addHome', 'VerifyController@addHome');

Route::resource('home', 'HomeController');
Route::get('test', 'WebHook@test');
Route::get('user/{userID}', 'UserController@user');

Route::post('edoc/{ui}', 'EdocumentController@requestEdoc');
Route::post('home/{ui}/del', 'HomeController@destroy');
Route::post('home/get/line_user', 'HomeController@getLineUserIdByUI');

Route::get('home/{ui}/history', 'HomeController@history');

Route::post('push/staff/{ui}', 'WebHook@loadStaffCard');

Route::middleware('optimizeImages')->group(function () {
    Route::post('report/outage', 'FFMController@reportOutage');
    Route::post('report/home', 'FFMController@reportHome');
});

Route::post('linepay/{ui}', 'LinepayController@index');
Route::get(
    'qrcode',
    'HomeController@qrGen'
);

Route::post(
    'kplus/{ui}/push',
    'KbankController@push'
);

Route::post(
    'payments/kbank/callback',
    'KbankController@callback'
);

Route::resource(
    'stats',
    'StatController'
);

Route::group(['prefix' => 'exports'], function () {
    Route::get('/', 'ExportController@index');
    Route::get('users', 'ExportController@users');
    Route::get('users-with-home', 'ExportController@usersWithHome');
    Route::get('homes', 'ExportController@homes');
});

Route::group(['prefix' => 'e-doc'], function () {
    Route::get('/check/{ui}', 'EbillConnectController@check');
    Route::get('/check-test/{ui}', 'EbillConnectTestController@check');
    Route::post('/create/{ui}', 'EbillConnectController@store');
    Route::post('/cancel/{ui}', 'EbillConnectController@cancelEdoc');
});

Route::get('clear', function () {
    return 'ok';
});

Route::post('warroom/close/sending', 'WebHook@sendCloseChatWarroom');

Route::post('otp', 'UserController@otp');
Route::post('otp_comfirm', 'UserController@confirmOtp');

Route::get('line/test', function () {
    $liff = phpinfo();

    // return $liff->get('reportOutage', ['meter_no' => 1]);

});
