<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKplusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpluses', function (Blueprint $table) {
            $table->id();
            $table->string('bill_id');
            $table->bigInteger('user_id');
            $table->longText('req');
            $table->longText('res')->nullable();
            $table->integer('status')->default(1)->comment('1. Pending 2.Success 3.Error');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpluses');
    }
}
