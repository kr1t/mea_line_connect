<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('tel')->unique()->nullable();
            $table->string('line_user_id')->unique()->nullable();
            $table->string('staff_id')->nullable();
            $table->string('agency_name')->nullable();
            $table->string('outsource_id')->nullable();
            $table->string('owner_name')->nullable();
            $table->string('owner_id')->nullable();
            $table->string('company_name')->nullable();
            $table->string('company_email')->nullable();
            $table->string('password')->nullable();
            $table->string('role')->nullable();
            $table->boolean('send_to_warroom')->default(0);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}