<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEbillLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ebill_logs', function (Blueprint $table) {
            $table->id();
            $table->boolean('status')->comment('0.ยกเลิก 1.สมัคร')->default(1);
            $table->bigInteger('user_id')->nullable();
            $table->string('ca')->nullable();
            $table->string('ui')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ebill_logs');
    }
}