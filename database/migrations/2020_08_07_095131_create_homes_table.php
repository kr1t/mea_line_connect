<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homes', function (Blueprint $table) {
            $table->id();
            $table->string('ca');
            $table->string('ui');
            $table->string('name');
            $table->bigInteger('user_id');
            $table->uuid('uuid');
            $table->string('ebill_email')->nullable();
            $table->dateTime('request_e_document_at')->comment('บันทึกเวลาที่ user กด ขอรับ e-documents หากเป็น null แปลว่า user ไม่ต้องการรับเอกสาร ขอรับใบแจ้งค่าไฟ ใบเสร็จรับเงิน หรือใบกำกับภาษี หนังสือเตือน หรือหนังสือแจ้งตัดค่าไฟฟ้า ในปัจจุบัน และอนาคต')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homes');
    }
}
