<?php

use Illuminate\Database\Seeder;
use App\Home;
use App\User;

class HomesTableSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();
        $homeDraf = [];

        for ($i = 0; $i < 75; $i++) {
            array_push($homeDraf, [
                "ca" => $faker->bankRoutingNumber,
                "ui" => '0' . $faker->bankRoutingNumber
            ]);
        }


        $runtime = 100;
        for ($i = 0; $i < $runtime; $i++) {

            $homeDrafKey = array_rand($homeDraf);
            $user_id = User::select('id')->get()->random(1)->pluck('id')[0];
            $checkInvalid = Home::where([
                ['ca', $homeDraf[$homeDrafKey]['ca']],
                ['ui', $homeDraf[$homeDrafKey]['ui']],
                ['user_id', $user_id]
            ])->first();

            if ($checkInvalid) {
                $runtime++;
                return false;
            }

            Home::create(
                [
                    'ca' => $homeDraf[$homeDrafKey]['ca'],
                    'ui' => $homeDraf[$homeDrafKey]['ui'],
                    'name' => $faker->city,
                    'user_id' => $user_id
                ]
            );
        }
    }
}
