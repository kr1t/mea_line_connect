<?php

use App\Stat;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $timesRand = [
            Carbon::now(),
            Carbon::now()->subDays(7),
            Carbon::now()->subMonths(1),
            Carbon::now()->subMonths(11),
            Carbon::now()->subMonths(12),
        ];

        $pageRand = [
            'ลงทะเบียนสมาชิก',
            'แจ้งไฟฟ้าขัดข้อง',
            'รับเอกสาร E-Document',
            'ชำระค่าไฟ',
            'เพิ่มบ้าน',
            'จัดการโปรไฟล์',
            'เข้าสู่ระบบสตาฟ',
        ];

        for ($i = 0; $i < 2500; $i++) {
            $timesRandKey = array_rand($timesRand);
            $pageRandKey = array_rand($pageRand);
            $user_id = User::select('id')->get()->random(1)->pluck('id')[0];

            Stat::insert([
                [
                    "ip" => $faker->ipv4,
                    "page_name" => $pageRand[$pageRandKey],
                    "user_id" => $user_id,
                    "created_at" => $timesRand[$timesRandKey],
                    "updated_at" => $timesRand[$timesRandKey],
                ],
            ]);
        }
    }
}
