<?php

use Illuminate\Database\Seeder;
use App\Config;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Config::insert([
            ['key' => 'staff_salary_link', 'value' => 'https://mea.or.th'],
            ['key' => 'staff_news_link', 'value' => 'https://mea.or.th'],
            ['key' => 'staff_hr_doc_link', 'value' => 'https://mea.or.th'],
            ['key' => 'outsource_sendwork_link', 'value' => 'https://mea.or.th'],
            ['key' => 'outsource_doc_link', 'value' => 'https://mea.or.th'],


        ]);
    }
}
