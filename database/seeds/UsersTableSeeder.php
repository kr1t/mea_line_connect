<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        User::create([
            "first_name" =>
            'admin', "last_name" =>
            'cms', "email" =>
            'admin@cms.com', 'tel' => '0964096784', 'line_user_id' => 'U2d9ef70b54f10f49329660b214bc1196', 'password' => bcrypt('123456'), 'role' => 'admin',
        ]);

        User::create([
            "first_name" => 'Admin', "last_name" => 'CMS', "email" =>
            'admin@measy.mea.or.th', 'password' => bcrypt('WgkF6hfN6urfI7ZwaIQx'), 'role' => 'admin',
        ]);

        for ($i = 0; $i < 20; $i++) {
            User::create([
                "first_name" => $faker->name,
                "last_name" => $faker->lastName,
                "email" => $faker->email,
                'tel' => $faker->phoneNumber,
                'line_user_id' => 'U' . str_replace('-', '', $faker->uuid),
                'password' => bcrypt('123456'),
            ]);
        }
    }
}
